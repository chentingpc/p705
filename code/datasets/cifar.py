import os
from functools import partial
import tensorflow as tf


_HEIGHT = 32
_WIDTH = 32
_DEPTH = 3
_NUM_CLASSES_TEN = 10
_NUM_CLASSES_HUNDRED = 100
_LABEL_BYTE_TEN = 1
_LABEL_BYTE_HUNDRED = 2
_NUM_DATA_FILES_TEN = 5

_NUM_IMAGES = {
    'train': 50000,
    'validation': 10000,
}


def record_dataset(filenames, is_ten):
  """Returns an input pipeline Dataset from `filenames`."""
  label_bytes = _LABEL_BYTE_TEN if is_ten else _LABEL_BYTE_HUNDRED
  record_bytes = _HEIGHT * _WIDTH * _DEPTH + label_bytes
  return tf.data.FixedLengthRecordDataset(filenames, record_bytes)


def get_filenames_10(is_training, data_dir):
  """Returns a list of filenames."""
  data_dir = os.path.join(data_dir, 'cifar-10-batches-bin')

  assert os.path.exists(data_dir), (
      'Run cifar10_download_and_extract.py first to download and extract the '
      'CIFAR-10 data.')

  if is_training:
    return [
        os.path.join(data_dir, 'data_batch_%d.bin' % i)
        for i in range(1, _NUM_DATA_FILES_TEN + 1)
    ]
  else:
    return [os.path.join(data_dir, 'test_batch.bin')]


def get_filenames_100(is_training, data_dir):
  """Returns a list of filenames."""
  data_dir = os.path.join(data_dir, 'cifar-100-binary')

  assert os.path.exists(data_dir), (
      'Download CIFAR-100 binary data first.')

  if is_training:
    return [os.path.join(data_dir, 'train.bin')]
  else:
    return [os.path.join(data_dir, 'test.bin')]



def parse_record(raw_record, is_ten, data_format):
  """Parse CIFAR-10 image and label from a raw record."""
  # Every record consists of a label followed by the image, with a fixed number
  # of bytes for each.
  label_bytes = _LABEL_BYTE_TEN if is_ten else _LABEL_BYTE_HUNDRED
  image_bytes = _HEIGHT * _WIDTH * _DEPTH
  record_bytes = label_bytes + image_bytes

  # Convert bytes to a vector of uint8 that is record_bytes long.
  record_vector = tf.decode_raw(raw_record, tf.uint8)

  # The first byte represents the label, which we convert from uint8 to int32
  # and then to one-hot.
  if is_ten:
    label = tf.cast(record_vector[0], tf.int32)
    label = tf.one_hot(label, _NUM_CLASSES_TEN)
  else:
    label = tf.cast(record_vector[1], tf.int32)
    label = tf.one_hot(label, _NUM_CLASSES_HUNDRED)

  # The remaining bytes after the label represent the image, which we reshape
  # from [depth * height * width] to [depth, height, width].
  depth_major = tf.reshape(
      record_vector[label_bytes:record_bytes], [_DEPTH, _HEIGHT, _WIDTH])

  # Convert from [depth, height, width] to [height, width, depth], and cast as
  # float32.
  image = tf.cast(tf.transpose(depth_major, [1, 2, 0]), tf.float32)

  """ Not compatible with ``preprocess_image``
  # The remaining bytes after the label represent the image.
  image = record_vector[label_bytes:record_bytes]
  if data_format != "flat":
    # from [depth * height * width] to [depth, height, width].
    image = tf.reshape(image, [_DEPTH, _HEIGHT, _WIDTH])
    if data_format == "channels_last":
      # Convert from [depth, height, width] to [height, width, depth]
      image = tf.transpose(depth_major, [1, 2, 0])
  image = tf.cast(image, tf.float32)
  """

  return image, label


def preprocess_image(image, is_training, preprocessing=False, standardization=False):
  """Preprocess a single image of layout [height, width, depth]."""
  if preprocessing and is_training:
    # Resize the image to add four extra pixels on each side.
    image = tf.image.resize_image_with_crop_or_pad(
        image, _HEIGHT + 8, _WIDTH + 8)

    # Randomly crop a [_HEIGHT, _WIDTH] section of the image.
    image = tf.random_crop(image, [_HEIGHT, _WIDTH, _DEPTH])

    # Randomly flip the image horizontally.
    image = tf.image.random_flip_left_right(image)

  if standardization:
    # Subtract off the mean and divide by the variance of the pixels.
    image = tf.image.per_image_standardization(image)
  else:
    image /= 255.
  return image


def input_fn(is_ten, is_training, data_dir, batch_size,
             num_epochs, reinitializer, data_format="flat"):
  """Input_fn using the tf.data input pipeline for CIFAR-10 dataset.
  Args:
    is_ten: Whether it is Cifar 10 or Cifar 100.
    is_training: A boolean denoting whether the input is for training.
    data_dir: The directory containing the input data.
    batch_size: The number of samples per batch.
    num_epochs: The number of epochs to repeat the dataset.
    data_format: one of "flat", "channels_first", "channels_last"
  Returns:
    A tuple of images and labels.
  """
  filename_func = get_filenames_10 if is_ten else get_filenames_100
  dataset = record_dataset(filename_func(is_training, data_dir), is_ten)

  if is_training:
    # When choosing shuffle buffer sizes, larger sizes result in better
    # randomness, while smaller sizes have better performance. Because CIFAR-10
    # is a relatively small dataset, we choose to shuffle the full epoch.
    dataset = dataset.shuffle(buffer_size=_NUM_IMAGES['train'])

  dataset = dataset.map(
      partial(parse_record, is_ten=is_ten, data_format=data_format))
  dataset = dataset.map(
      lambda image, label: (preprocess_image(image, is_training), label))

  dataset = dataset.prefetch(2 * batch_size)

  # We call repeat after shuffling, rather than before, to prevent separate
  # epochs from blending together.
  dataset = dataset.repeat(num_epochs)

  # Batch results by up to batch_size, and then fetch the tuple from the
  # iterator.
  dataset = dataset.batch(batch_size)

  if reinitializer:
    iterator = tf.data.Iterator.from_structure(
        dataset.output_types, dataset.output_shapes)
    initializer = iterator.make_initializer(dataset)
    images, labels = iterator.get_next()
  else:
    images, labels = dataset.make_one_shot_iterator().get_next()
    initializer = None

  if data_format != "channels_last":
    if data_format == 'flat':
      images = tf.reshape(images, [-1, _HEIGHT * _WIDTH * _DEPTH])
    else:
      assert data_format == 'channels_first'
      images = tf.transpose(images, [0, 3, 1, 2])
    
  return images, labels, initializer


if __name__ == "__main__":
  is_ten = False
  is_training = True
  data_dir = "/tmp/cifar10_data"
  data_dir = "/tmp/cifar10_data"
  batch_size = 128
  reinitializer = True
  X, y, initializer = input_fn(
      is_ten, is_training, data_dir, batch_size,
      num_epochs=1, reinitializer=reinitializer)
  with tf.Session() as sess:
    if reinitializer:
      sess.run(initializer)
    while True:
      results = sess.run([X, y])
      break
  import pdb
  pdb.set_trace()
