#  Copyright 2018 The TensorFlow Authors. All Rights Reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
"""tf.data.Dataset interface to the MNIST dataset."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import shutil
import gzip
import numpy as np
from six.moves import urllib
import tensorflow as tf


def read32(bytestream):
  """Read 4 bytes from bytestream as an unsigned 32-bit integer."""
  dt = np.dtype(np.uint32).newbyteorder('>')
  return np.frombuffer(bytestream.read(4), dtype=dt)[0]


def check_image_file_header(filename):
  """Validate that filename corresponds to images for the MNIST dataset."""
  with tf.gfile.Open(filename,'rb') as f:
    magic = read32(f)
    num_images = read32(f)
    rows = read32(f)
    cols = read32(f)
    if magic != 2051:
      raise ValueError('Invalid magic number %d in MNIST file %s' % (magic,
                                                                     f.name))
    if rows != 28 or cols != 28:
      raise ValueError(
          'Invalid MNIST file %s: Expected 28x28 images, found %dx%d' %
          (f.name, rows, cols))


def check_labels_file_header(filename):
  """Validate that filename corresponds to labels for the MNIST dataset."""
  with tf.gfile.Open(filename,'rb') as f:
    magic = read32(f)
    num_items = read32(f)
    if magic != 2049:
      raise ValueError('Invalid magic number %d in MNIST file %s' % (magic,
                                                                     f.name))


def download(directory, filename):
  """Download (and unzip) a file from the MNIST dataset, if it doesn't already exist."""
  filepath = os.path.join(directory, filename)
  if tf.gfile.Exists(filepath):
    return filepath
  if not tf.gfile.Exists(directory):
    tf.gfile.MakeDirs(directory)
  # CVDF mirror of http://yann.lecun.com/exdb/mnist/
  url = 'https://storage.googleapis.com/cvdf-datasets/mnist/' + filename + '.gz'
  zipped_filepath = filepath + '.gz'
  print('Downloading %s to %s' % (url, zipped_filepath))
  urllib.request.urlretrieve(url, zipped_filepath)
  with gzip.open(zipped_filepath, 'rb') as f_in, open(filepath, 'wb') as f_out:
    shutil.copyfileobj(f_in, f_out)
  os.remove(zipped_filepath)
  return filepath


def dataset(directory, images_file, labels_file):
  images_file = download(directory, images_file)
  labels_file = download(directory, labels_file)

  check_image_file_header(images_file)
  check_labels_file_header(labels_file)

  def decode_image(image):
    # Normalize from [0, 255] to [0.0, 1.0]
    image = tf.decode_raw(image, tf.uint8)
    image = tf.cast(image, tf.float32)
    image = tf.reshape(image, [784])
    return image / 255.0

  def one_hot_label(label):
    label = tf.decode_raw(label, tf.uint8)  # tf.string -> tf.uint8
    label = tf.reshape(label, [])  # label is a scalar
    return tf.one_hot(label, 10)

  images = tf.data.FixedLengthRecordDataset(
      images_file, 28 * 28, header_bytes=16).map(decode_image)
  labels = tf.data.FixedLengthRecordDataset(
      labels_file, 1, header_bytes=8).map(one_hot_label)
  return tf.data.Dataset.zip((images, labels))


def train(directory):
  """tf.data.Dataset object for MNIST training data."""
  return dataset(os.path.join(directory, "binary"),
                 'train-images-idx3-ubyte', 'train-labels-idx1-ubyte')


def test(directory):
  """tf.data.Dataset object for MNIST test data."""
  return dataset(os.path.join(directory, "binary"),
                 't10k-images-idx3-ubyte', 't10k-labels-idx1-ubyte')


def input_fn(is_training, data_dir, batch_size,
             num_epochs, reinitializer, data_format="flat"):
  """Input_fn using the tf.data input pipeline for Mnist dataset.
  Args:
    is_training: A boolean denoting whether the input is for training.
    data_dir: The directory containing the input data.
    batch_size: The number of samples per batch.
    num_epochs: The number of epochs to repeat the dataset.
    data_format: one of "flat", "channels_first", "channels_last"
  Returns:
    A tuple of images and labels.
  """
  if is_training:
    dataset = train(data_dir)
    dataset = dataset.cache()
    dataset = dataset.shuffle(buffer_size=50000)
  else:
    dataset = test(data_dir)
    dataset = dataset.cache()

  dataset = dataset.batch(batch_size).repeat(num_epochs)

  if reinitializer:
    iterator = tf.data.Iterator.from_structure(
        dataset.output_types, dataset.output_shapes)
    initializer = iterator.make_initializer(dataset)
    images, labels = iterator.get_next()
  else:
    images, labels = dataset.make_one_shot_iterator().get_next()
    initializer = None

  if data_format != "flat":
    if data_format == 'channels_first':   # Faster for GPUs.
      _input_shape = [-1, 1, 28, 28]
    else:
      assert data_format == 'channels_last'  # Faster for CPUs.
      _input_shape = [-1, 28, 28, 1]
    images = tf.reshape(images, _input_shape)

  return images, labels, initializer


if __name__ == "__main__":
  is_training = True
  data_dir = "/tmp/mnist"
  batch_size = 128
  reinitializer = True
  X, y, initializer = input_fn(
      is_training, data_dir, batch_size,
      num_epochs=100, reinitializer=reinitializer)
  # import time
  # start = time.time()
  with tf.Session() as sess:
    if reinitializer:
      sess.run(initializer)
    while True:
      results = sess.run([X, y])
      print("ss")
      break
  # print(time.time() - start)
  import pdb
  pdb.set_trace()
