import os
from functools import partial
import tensorflow as tf


_SIFT_FEAT_DIM = 128
_GIST_FEAT_DIM = 960
_HEAD_BYTE = 4
_SIFT_MAIN_BYTE = _SIFT_FEAT_DIM * 4
_GIST_MAIN_BYTE = _GIST_FEAT_DIM * 4
_SHUFFLE_SIZE = 50000


def record_dataset(filenames, data_name):
  """Returns an input pipeline Dataset from `filenames`."""
  if data_name.startswith("sift"):
    main_byte = _SIFT_MAIN_BYTE
  else:
    assert data_name.startswith("gist")
    main_byte = _GIST_MAIN_BYTE
  record_bytes = _HEAD_BYTE + main_byte
  return tf.data.FixedLengthRecordDataset(filenames, record_bytes)


def get_filenames(is_training, data_dir):
  """Returns a list of filenames."""

  if is_training:
    return [os.path.join(data_dir, "learn.fvecs")]
  else:
    return [os.path.join(data_dir, "query_n_base.fvecs")]


def parse_record(raw_record, data_name):
  record_vector = tf.decode_raw(raw_record, tf.float32)
  if data_name.startswith("sift"):
    feat_dim = _SIFT_FEAT_DIM
  else:
    assert data_name.startswith("gist")
    feat_dim = _GIST_FEAT_DIM
  feature = tf.reshape(record_vector[1:feat_dim+1], [feat_dim])

  return feature


def preprocess(features, data_name, is_training):
  return features


def input_fn(data_name, is_training, data_dir, batch_size,
             num_epochs, reinitializer):
  """Input_fn using the tf.data input pipeline.
  Args:
    data_name: one of "sift10k", "sift1m", "gist1m".
    is_training: A boolean denoting whether the input is for training.
    data_dir: The directory containing the input data.
    batch_size: The number of samples per batch.
    num_epochs: The number of epochs to repeat the dataset.
  Returns:
    features.
  """
  data_name = data_name.lower()
  dataset = record_dataset(get_filenames(is_training, data_dir), data_name)

  if is_training:
    dataset = dataset.shuffle(buffer_size=_SHUFFLE_SIZE)

  dataset = dataset.map(
      partial(parse_record, data_name=data_name), num_parallel_calls=1)
  #dataset = dataset.map(
  #    lambda feature: (preprocess(feature, data_name, is_training)))

  dataset = dataset.prefetch(2 * batch_size)

  # We call repeat after shuffling, rather than before, to prevent separate
  # epochs from blending together.
  dataset = dataset.repeat(num_epochs)

  dataset = dataset.batch(batch_size)

  if reinitializer:
    iterator = tf.data.Iterator.from_structure(
        dataset.output_types, dataset.output_shapes)
    initializer = iterator.make_initializer(dataset)
    features = iterator.get_next()
  else:
    features = dataset.make_one_shot_iterator().get_next()
    initializer = None

  return features, initializer


if __name__ == "__main__":
  is_training = True
  data_name = "sift1m"
  data_dir = "~/dbase/image/ann/"
  data_dir = "/home/chentingpc/Downloads/ann/%s/" % data_name
  batch_size = 256
  reinitializer = True
  X, initializer = input_fn(
      data_name, is_training, data_dir, batch_size,
      num_epochs=100, reinitializer=reinitializer)
  with tf.Session() as sess:
    if reinitializer:
      sess.run(initializer)
    import time
    start = time.time()
    # while True:
    for _ in range(5000):
      #results = sess.run([X, y])
      results = sess.run(X)
      #print("ss")
      #break
    print(time.time() - start)
  import pdb
  pdb.set_trace()
