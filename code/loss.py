import tensorflow as tf
import util
log = util.safer_log


def get_gan_loss(loss_type, score_real, score_fake, D, real, fake):
  if loss_type == "logistic":
    dis_loss = - tf.reduce_mean(log(score_real) + log(1. - score_fake))
    gen_loss = - tf.reduce_mean(log(score_fake))
  elif loss_type == "ls":
    dis_loss = tf.reduce_mean(
        tf.square(score_real - 0.) + (tf.square(score_fake - 1.)))
    gen_loss = tf.reduce_mean(
        tf.square(score_fake - 0.))
  elif loss_type == "imwass":
    imwass_lambda = 10.  # TODO: add to hparams.
    i_ndims = real.shape.ndims
    mixing_factors = tf.random_uniform(
        [tf.shape(real)[0]] + [1] * (i_ndims - 1))
    mixed = real * (1. - mixing_factors) + fake * mixing_factors
    mixed_grad = tf.gradients(D(mixed), [mixed])[0]
    mixed_grad_norm = tf.sqrt(
        tf.reduce_sum(tf.square(mixed_grad), range(1, i_ndims)))
    grad_norm_penalty = tf.reduce_mean(tf.square(mixed_grad_norm - 1.))
    dis_loss = tf.reduce_mean(score_fake - score_real)
    dis_loss += imwass_lambda * grad_norm_penalty
    # Additionally, keep the scores from drifting too far from zero.
    #dis_loss += tf.reduce_mean(tf.square(score_real)) * 1e-3
    gen_loss = - tf.reduce_mean(score_fake)
  else:
    raise ValueError("Unknown loss type {}.".format(loss_type))

  return dis_loss, gen_loss
