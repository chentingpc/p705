import time
from numba import jit
from collections import Counter
import numpy as np
import sklearn
from sklearn import metrics
from sklearn.cluster import KMeans
import tensorflow as tf
import util


def kmeans_eval(X, y, rand_seed=42):
  """Evaluation based on K-means clustering of X.

  Note: not suitable for discrete data.

  Args:
    X: (N, d) data.
    y: (N, ) label.
  """
  k = len(set(y))
  kmeans = KMeans(n_clusters=k, random_state=rand_seed)
  yp = kmeans.fit_predict(X)

  nmi = metrics.normalized_mutual_info_score(y, yp)
  adjusted_nmi = metrics.adjusted_mutual_info_score(y, yp)
  adjusted_rand = metrics.adjusted_rand_score(y, yp)
  results = {"nmi": nmi,
             "adjusted_nmi": adjusted_nmi,
             "adjusted_rand": adjusted_rand}
  return results


def l2_normalize(X):
  """Row-wise l2 normalize N * d data X matrix"""
  return X / np.sqrt(np.sum(X**2, axis=1, keepdims=True) + util.eps_micro)


def euclidean_distance(X, query_ids=None, min_diagonal=True):
  """Compute euclidean_distance between every pairs of X[query_ids] and X."""
  # start = time.time()
  if query_ids is None:
    row_sum = np.sum(X**2, axis=1, keepdims=True)
    N_dist = row_sum - 2. * X.dot(X.T) + row_sum.T
  else:
    row_sum_query = np.sum(X[query_ids]**2, axis=1, keepdims=True)
    row_sum = np.sum(X**2, axis=1, keepdims=True)
    N_dist = row_sum_query - 2. * X[query_ids].dot(X.T) + row_sum.T
  if min_diagonal:
    np.fill_diagonal(N_dist, N_dist.min() - 1)
  # print time.time() - start
  return N_dist


def find_knns(X, k, add_tiny_noises=True, query_ids=None):
  """Returns q x (K+1) index array of nearest neighbors in Euclidean distance.
      The additional one is the self idx.
  """
  N_dist = euclidean_distance(X, query_ids)
  N_dist += np.random.random(N_dist.shape) * 1e-9
  # neighbors = np.argsort(N_dist, axis=1)[:, :k+1]
  neighbors = np.argpartition(N_dist, k+1)[:, :k+1]
  return neighbors


def knn_eval_by_labels(X, y, ks, query_ids=None, return_cases=(10, 10)):
  """Evaluation based on nearest neighbors' labels.

  X are candidates, subset of X will be used as queries as well depending on
  query_ids. The hit/miss is computed based on whether or not the query and
  target share the same label. The distance matrix is Euclidean for continuous
  X but Hamming distance for discrete X.

  Args:
    X: (N, d) data.
    y: (N, ) label, or (N, t) idxs of true neighbors.
    ks: a list of k used to compute the metrics.
    query_ids: a list of ids of X's rows as queries, otherwise all rows are.
    return_cases: (num_q, num_nb), if specified, will return neighbor random 
      sampled neighbor idxs in shape of (num_q, 1 + num_nb), where the first
      column is the id of query, others are ids of the neighbors.
  """
  # TODO: replace with mini-batch implementation.
  if X.dtype.name.startswith("int"):
    distance = "Hamming"
  else:
    distance = "Euclidean"
  if not isinstance(ks, list):
    ks = [ks]
  ks = sorted(ks)
  max_k = ks[-1]
  query_size = X.shape[0] if query_ids is None else len(query_ids)
  y = np.array(y)
  y_is_label = len(y.shape) == 1

  # Compute pairwise distances N_dist (M, N) from query_ids to all data.
  if distance == "Euclidean":
    N_dist = euclidean_distance(X, query_ids, min_diagonal=False)
  else:  # Hamming.
    # start = time.time()
    if True:  # This implementation is faster compared to the alternative.
      if query_ids is None:
        h_dists = pairwise_hamming(X)
      else:
        h_dists = pairwise_hamming2(X[query_ids], X)
    else:
      if query_ids is None:
        X_q = np.repeat(X, X.shape[0], axis=0)
      else:
        X_q = np.repeat(X[query_ids], X.shape[0], axis=0)
      X_t = np.tile(X, (query_size, 1))
      if X_q.shape != X_t.shape:
        raise ValueErorr("Shapes do not match for Hamming distance computation")
      h_dists = np.sum(X_q != X_t, axis=1)
    N_dist = h_dists.reshape((query_size, X.shape[0]))
    assert np.mean(np.diag(N_dist)) == 0  # Due to self relation, sanity check
    # print time.time() - start
  # N_dist *= 0  # Sanity check baseline.
  # np.random.shuffle(N_dist)  # Sanity check baseline.
  # N_dist += np.random.random(N_dist.shape) * 1e-4  # small permutation.
  np.fill_diagonal(N_dist, N_dist.min() - 1)

  # Create (M, N) neighbor matrix, each row sorted by increasing distance.
  Nb = np.argsort(N_dist, axis=1)  # (M, N)
  # Create (M, N) binary matrix indicating hit.
  if y_is_label:  # y is label of x.
    if query_ids is None:
      query_labels = y.reshape((y.shape[0], 1))
    else:
      query_labels = y[query_ids].reshape((len(query_ids), 1))
    Nb_hit = (query_labels == y[Nb]).astype(np.float32)
  else:  # y are true neighbors' idxs
    idxs_inc = (np.arange(N_dist.shape[0]) * N_dist.shape[1]).reshape(
        (N_dist.shape[0], 1))
    Nb_hit = np.zeros_like(N_dist)
    np.put(Nb_hit, y + idxs_inc, 1.)
    Nb_hit = np.take(Nb_hit, Nb + idxs_inc)

  # Compute the metrics based on neighbor hit matrix Nb_hit.
  precisions = []
  recalls = []
  candidates_m1 = Nb_hit.shape[1] - 1
  all_true_hits = np.sum(Nb_hit, axis=1, keepdims=True) - 1  # Remove itself.
  for k in ks:
    num_hits = np.sum(Nb_hit[:, :k+1], axis=1, keepdims=False) - 1  # Remove itself.
    _precision = np.mean(num_hits) / min(k, candidates_m1)
    _recall = np.mean(num_hits / all_true_hits)
    assert _precision >= 0, [_precision, k]
    assert _recall >= 0, [_recall, k]
    precisions.append(_precision)
    recalls.append(_recall)
  precisions.append(min(np.mean(all_true_hits) / min(max_k, candidates_m1), 1))
  recalls.append(min(min(max_k, candidates_m1) / np.mean(all_true_hits), 1))

  # Get some cases back.
  if return_cases is not None:
    num_q, num_nb = return_cases
    rnd_ixds = np.random.randint(0, X.shape[0], num_q)
    nn_cases = Nb[rnd_ixds][:, :num_nb+1]  #(num_q, 1 + num_nb), first column is the query.
  else:
    nn_cases = None

  results = {"precisions": precisions,
             "recalls": recalls,
             "nn_cases": nn_cases}

  return results


def hknn_eval_by_labels(
      X, y, ks, reverse_order=True, query_ids=None, return_cases=(10, 10)):
  """Evaluation based on nearest neighbors' labels.

  Similar to ``knn_eval_by_labels``, but only works for discrete code. Measure
  distance using the tree span by the prefix of discrete codes. This is done
  by simply sorting the codes, and retrieve neighbors 1-dimensional neighbors.

  Args:
    reverse_order: whther or not to reverse the order of the code.
    y: (N, ) label, or (N, t) idxs of true neighbors.
    return_cases: (num_q, num_nb), if specified, will return neighbor random 
      sampled neighbor idxs in shape of (num_q, 1 + num_nb), where the first
      column is the id of query, others are ids of the neighbors.
  """
  if not isinstance(ks, list):
    ks = [ks]
  max_k = ks[-1]
  # Obtain sorted_labels (and query_ids_new if query_ids is given)
  if reverse_order:
    X = X[:, range(X.shape[1])[::-1]]
  #X = X[:, 1:]  # DEBUG
  X_aug = X
  # X_aug = np.random.random((X.shape[0], 1))
  # X_aug = np.hstack((X, X_aug))
  # X_aug *= 0  # Sanity check baseline.
  # np.random.shuffle(X_aug)# Sanity check baseline.
  y = np.array(y)
  y_is_label = len(y.shape) == 1
  y_loc = range(y.shape[0])
  if query_ids is None:
    sorted_Xy = sorted(zip(X_aug.tolist(), y_loc))
    sorted_X = [X_ for X_, _ in sorted_Xy]
    sorted_y_loc = [y_loc_ for _, y_loc_ in sorted_Xy]
    sorted_labels =  y[sorted_y_loc].tolist()
    query_ids_new = range(y.shape[0])
    label_ids = sorted_y_loc
  else:
    raise NotImplemented()

  @jit
  def _prefix_similarity(x, y, length):
    sim = 0
    for i in range(length):
      if x[i] == y[i]:
        sim += 1
      else:
        break
    return sim

  @jit
  def _compute_metrics_on_sorted_labels(
        codes, labels, query_ids, ks, label_alpossible_hits):
    """
      query_ids is a list of size of queries, pointing to query locs in labels.
      labels is a list of labels, should only be used when y_is_label is True.
      label_alpossible_hits is dict of total number of trues for a given label.
    """
    # TODO: this is too expensive to compute for large k!
    N = len(query_ids)  # num queries.
    M = len(labels)  # num candidates.
    D = len(codes[0])  # code length.
    ks = sorted(ks)
    max_k = max(ks)
    ks_size = len(ks)
    precisions, recalls = [0.0] * ks_size, [0.0] * ks_size
    for i in range(N):
      hit = 0.
      qid = query_ids[i]
      qcode = codes[i]
      target = labels[qid]
      num_nb_found = 0
      next_k_idx = 0
      next_k = ks[next_k_idx]
      ptr1, ptr1_dist, ptr1_done = qid - 1, 999, False
      ptr2, ptr2_dist, ptr2_done = qid + 1, 999, False
      while num_nb_found < max_k:
        if ptr1 < 0:
          ptr1_dist = 999
          ptr1_done = True
        else:
          ptr1_dist = -_prefix_similarity(codes[ptr1], qcode, D)
        if ptr2 >= M:
          ptr2_dist = 999
          ptr2_done = True
        else:
          ptr2_dist = -_prefix_similarity(codes[ptr2], qcode, D)
        if ptr1_done and ptr2_done:
          break
        if ptr1_dist >= ptr2_dist:
          choose = ptr2
          ptr2 += 1
        else:
          choose = ptr1
          ptr1 -= 1
        hit += (labels[choose] == target)
        num_nb_found += 1
        if num_nb_found == next_k:
          precisions[next_k_idx] += hit / num_nb_found / N
          recalls[next_k_idx] += hit / (label_alpossible_hits[target] - 1) / N
          next_k_idx = (next_k_idx + 1) % ks_size
          next_k = ks[next_k_idx]
    return precisions, recalls

  @jit
  def _compute_metrics_on_sorted_labels2(
        codes, labels, label_ids, query_ids, ks):
    """
      Different from ``_compute_metrics_on_sorted_labels``, where labels is
      a list of labels, here it is a list of lists of true neighbors' idxs.
      query_ids here means differently, assuming all labels are queries here,
    """
    # TODO: this is too expensive to compute for large k!
    N = len(query_ids)  # num queries.
    M = len(labels)  # num candidates.
    D = len(codes[0])  # code length.
    ks = sorted(ks)
    max_k = max(ks)
    ks_size = len(ks)
    precisions, recalls = [0.0] * ks_size, [0.0] * ks_size
    for i in range(N):
      hit = 0.
      qid = query_ids[i]
      qcode = codes[i]
      targets = set(labels[qid])
      num_nb_found = 0
      next_k_idx = 0
      next_k = ks[next_k_idx]
      ptr1, ptr1_dist, ptr1_done = qid - 1, 999, False
      ptr2, ptr2_dist, ptr2_done = qid + 1, 999, False
      while num_nb_found < max_k:
        if ptr1 < 0:
          ptr1_dist = 999
          ptr1_done = True
        else:
          ptr1_dist = -_prefix_similarity(codes[ptr1], qcode, D)
        if ptr2 >= M:
          ptr2_dist = 999
          ptr2_done = True
        else:
          ptr2_dist = -_prefix_similarity(codes[ptr2], qcode, D)
        if ptr1_done and ptr2_done:
          break
        if ptr1_dist >= ptr2_dist:
          choose = ptr2
          ptr2 += 1
        else:
          choose = ptr1
          ptr1 -= 1
        hit += label_ids[choose] in targets
        num_nb_found += 1
        if num_nb_found == next_k:
          precisions[next_k_idx] += hit / num_nb_found / N
          recalls[next_k_idx] += hit / len(targets) / N
          next_k_idx = (next_k_idx + 1) % ks_size
          next_k = ks[next_k_idx]
    return precisions, recalls

  # check query_ids_new, and sorted_labels.
  # start = time.time()
  if y_is_label:
    label_alpossible_hits = Counter(sorted_labels)
  else:
    label_alpossible_hits = None
  if y_is_label:
    precisions, recalls = _compute_metrics_on_sorted_labels(
        sorted_X, sorted_labels, query_ids_new, ks, label_alpossible_hits)
  else:
    precisions, recalls = _compute_metrics_on_sorted_labels2(
        sorted_X, sorted_labels, label_ids, query_ids_new, ks)
  # Compute the maximum of precision and recall.
  if y_is_label:
    query_labels = Counter(np.array(sorted_labels)[query_ids_new])
    max_precision = 0.
    max_recall = 0.
    candidates_m1 = len(sorted_labels) - 1
    for label, count in query_labels.iteritems():
      true_count = label_alpossible_hits[label] - 1.0
      max_precision += count * min(true_count / min(max_k*2, candidates_m1), 1)
      max_recall += count * min(min(max_k*2, candidates_m1) / true_count, 1)
    precisions.append(max_precision / len(query_ids_new))
    recalls.append(max_recall / len(query_ids_new))
  else:
    candidates_m1 = len(sorted_labels) - 1
    total_true = y.shape[1] - 1.0  # excluding itself.
    precisions.append(min(total_true / min(max_k*2, candidates_m1), 1))
    recalls.append(min(min(max_k*2, candidates_m1) / total_true, 1))
  # print time.time() - start

  # Get some cases back.
  if return_cases is not None:
    num_q, num_nb = return_cases
    assert num_nb % 2 == 0
    num_nb_oneside = num_nb // 2
    assert num_nb + num_q < X.shape[0]
    rnd_ixds = np.random.randint(
        num_nb_oneside, X.shape[0] - num_nb_oneside, num_q)
    nn_cases = []
    for q in rnd_ixds:
      nn_cases.append(sorted_y_loc[q - num_nb_oneside: q + num_nb_oneside + 1])
    nn_cases = np.array(nn_cases)
    idxs = [num_nb_oneside]
    for i in range(1, num_nb_oneside + 1):
      idxs += [num_nb_oneside + i, num_nb_oneside - i]
    nn_cases = nn_cases[:, idxs]  #(num_q, 1 + num_nb), first column is the query.
  else:
    nn_cases = None


  results = {"precisions": precisions,
             "recalls": recalls,
             "nn_cases": nn_cases}
  return results


def knn_eval_by_continuous(X, y, k):
  """Evaluation based on if continuous nearest neighbors in discrete nns.
  TODO.
  """
  pass


@jit
def pairwise_hamming(X):
  n = X.shape[0]
  dists = [0.] * (n**2)
  k = -1
  for i in range(n):
    for j in range(n):
      k += 1
      dists[k] = (X[i] != X[j]).sum()
  return np.array(dists)


@jit
def pairwise_hamming2(X, Y):
  n, m = X.shape[0], Y.shape[0]
  dists = [0.] * (n*m)
  k = -1
  for i in range(n):
    for j in range(m):
      k += 1
      dists[k] = (X[i] != Y[j]).sum()
  return np.array(dists)
