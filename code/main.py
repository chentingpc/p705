import os
import time
import numpy as np
from functools import partial
from sklearn.decomposition import PCA
import tensorflow as tf

import data
import sdata
import models
import metrics
import util

flags = tf.flags
FLAGS = flags.FLAGS
flags.DEFINE_string("task_name", "mnist", "")
flags.DEFINE_string("data_dir", "/tmp/tf/test/", "")
flags.DEFINE_string("save_sample_dir", ".", "")
flags.DEFINE_integer("num_data_points", 10000, "")
flags.DEFINE_integer("data_dim", 100, "")
flags.DEFINE_integer("num_clusters", 10, "")
flags.DEFINE_float("cluster_std", 0.1, "")
flags.DEFINE_float("train_test_split", 0.8, "")
flags.DEFINE_integer("data_gen_seed", 42, "")
flags.DEFINE_string("save_dir", "/tmp/tf/test/", "")
flags.DEFINE_bool("test_summary", False, "")
flags.DEFINE_bool("hierarchical_eval", False, "")
flags.DEFINE_bool("hierarchical_and_flat_eval", False, "")
flags.DEFINE_integer("use_raw_data_distance_for_eval", 0,
                     "When bigger than 0, use that amount of nearest neighbors "
                     "in original euclidean data space as true relevant data.")
flags.DEFINE_integer("plot_batch_size", 64, "")
flags.DEFINE_bool("plot_nn_cases", True, "If to plot the nearest neighbors "
                  "query-result images for case study.")
flags.DEFINE_bool("do_evaluation", True, "")
flags.DEFINE_bool("do_plot_data", True, "")

flags.DEFINE_string("loss", "logistic",
                    "Type of GAN value function: logistic, imwass, ls")
flags.DEFINE_integer("max_iterations", 100000,
                     "In terms of generator.")
flags.DEFINE_integer("num_iters_D", 5,
                     "Number of iterations to train D before train G.")
flags.DEFINE_integer("verbose_iters", 1000,
                     "Number of iterations before verbose.")
flags.DEFINE_integer("batch_size", 256, "")
flags.DEFINE_bool("conv_deconv_baby", False, "")
flags.DEFINE_string("model_family", None, "")
flags.DEFINE_string("sample_images_type", "random",
                    "The type of images to sample: random, reconst")
flags.DEFINE_integer("num_test_samples", None,
                    "Number of test samples to be used. Read sequentially! "
                    "Shouldn't be larger than all test samples!")
flags.DEFINE_string("code_label_filename", "code_label",
                    "The filename for saving the label\tcode.")
flags.DEFINE_string("optimizer", "adam", "")
flags.DEFINE_float("learning_rate", 1e-3, "")
flags.DEFINE_float("max_grad_norm", 100., "")

flags.DEFINE_bool("solo_dim_prior_sampling", False, "")
flags.DEFINE_bool("Z_use_label", False, "")
flags.DEFINE_integer("noises_dim", 10, "")
flags.DEFINE_string("auto_prototype", "vae", "prototype for auto_encoding.")
flags.DEFINE_bool("Z_from_X", True, "")
flags.DEFINE_integer("Z_sampling", 1, "The option for sampling Z.")
flags.DEFINE_bool("Z_batch_norm", False, "If or not to use batch_norm for Z.")
flags.DEFINE_float("Z_use_gan", 0, "")
flags.DEFINE_float("Z_use_KL", 0, "")
flags.DEFINE_bool("Z_soft_KL", False, "")
flags.DEFINE_float("X_use_gan", 0, "")
flags.DEFINE_float("X_use_reconst", 0, "")
flags.DEFINE_float("XZ_joint_gan", 0, "")
flags.DEFINE_string("noise_transform", "none", "")
flags.DEFINE_string("discrete_coding_scheme", "flat", "")
flags.DEFINE_integer("Z_discrete_K", 10, "K in KD code.")
flags.DEFINE_integer("Z_discrete_D", 0, "K in KD code.")
flags.DEFINE_integer("Z_d", 100, "The dimension of code embedding.")
flags.DEFINE_string("policy_optimizer", "adam", "")
flags.DEFINE_float("policy_learning_rate", 1e-3, "")
flags.DEFINE_float("policy_var_learning_rate", 1e-3, "")
flags.DEFINE_bool("policy_control_variate", True, "")
flags.DEFINE_integer("pg_num_samples", 1, "Number of samples for PG.")
flags.DEFINE_string("discrete_estimator", "gumbel", "Estimator/method.")
flags.DEFINE_float("vq_reg_weight", 1, "")
flags.DEFINE_bool("vq_shared_codebook", False, "")
flags.DEFINE_string("vq_code_emb_merger", "average", "")
flags.DEFINE_bool("Z_nested_dropout", False, "")

flags.DEFINE_integer("eps2Z_layers", 2, "")
flags.DEFINE_integer("eps2Z_hdim", 512, "")
flags.DEFINE_float("eps2Z_dropout", 0, "")
flags.DEFINE_string("eps2Z_hactv", "leaky_relu", "")
flags.DEFINE_integer("X2Zvq_layers", 2, "")
flags.DEFINE_integer("X2Zvq_hdim", 512, "")
flags.DEFINE_float("X2Zvq_dropout", 0, "")
flags.DEFINE_string("X2Zvq_hactv", "leaky_relu", "")
flags.DEFINE_integer("X2Z_layers", 2, "")
flags.DEFINE_integer("X2Z_hdim", 512, "")
flags.DEFINE_float("X2Z_dropout", 0, "")
flags.DEFINE_string("X2Z_hactv", "leaky_relu", "")
flags.DEFINE_integer("Z2X_layers", 2, "")
flags.DEFINE_integer("Z2X_hdim", 512, "")
flags.DEFINE_float("Z2X_dropout", 0, "")
flags.DEFINE_string("Z2X_hactv", "leaky_relu", "")
flags.DEFINE_string("Z2X_xactv", "linear", "")
flags.DEFINE_integer("D_layers", 2, "")
flags.DEFINE_integer("D_hdim", 512, "")
flags.DEFINE_float("D_dropout", 0, "")
flags.DEFINE_string("D_hactv", "leaky_relu", "")
flags.DEFINE_float("weight_supervised_loss", 0, "")
flags.DEFINE_integer("Z2Y_layers", 0, "")
flags.DEFINE_integer("Z2Y_hdim", 512, "")
flags.DEFINE_float("Z2Y_dropout", 0, "")
flags.DEFINE_string("Z2Y_hactv", "leaky_relu", "")

# Get data.
task_type = None
plog_rgb = False
if FLAGS.task_name in ["mnist", "cifar10", "cifar100"]:
  task_type = "images"
  _data_format = "flat"
  if FLAGS.task_name.startswith("cifar"):
    plog_rgb = True
  data_train, labels_train, _ = data.get_image_data(
      FLAGS.task_name, FLAGS.data_dir, True, FLAGS.batch_size,
      num_epochs=None, reinitializer=False, data_format=_data_format)
  data_test, labels_test, test_initializer = data.get_image_data(
      FLAGS.task_name, FLAGS.data_dir, False, FLAGS.batch_size,
      num_epochs=1, reinitializer=True, data_format=_data_format)
elif FLAGS.task_name in ["sift10k", "sift1m", "gist1m"]:
  task_type = "features"
  data_train, labels_train, _ = data.get_feature_data(
      FLAGS.task_name, FLAGS.data_dir, True, FLAGS.batch_size,
      num_epochs=None, reinitializer=False)
  data_test, labels_test, test_initializer = data.get_feature_data(
      FLAGS.task_name, FLAGS.data_dir, False, FLAGS.batch_size * 5,  # Faster code inference.
      num_epochs=1, reinitializer=True)
else:
  task_type = "synt_cloud"
  data = sdata.EmbeddingData(vocab_size=FLAGS.num_data_points,
                             method=FLAGS.task_name,
                             emb_dim=FLAGS.data_dim,
                             num_clusters=FLAGS.num_clusters,
                             cluster_std=FLAGS.cluster_std,
                             train_test_split=FLAGS.train_test_split,
                             normalize=False,
                             seed=FLAGS.data_gen_seed)
  idxs_train, data_train, labels_train, _ = data.batch(  # These are not one-hot labels.
      batch_size=FLAGS.batch_size, shuffle=True, is_train=True)
  idxs_test, data_test, labels_test, test_initializer = data.batch(
      batch_size=FLAGS.batch_size, shuffle=False, is_train=False,
      repeat=1, reinitializer=True)
  filename = os.path.join(FLAGS.save_sample_dir, "data_original")
  FLAGS.data_dim = data.d
  data.plot(filename=filename)

# Setup hyper-parameters.
hparams = tf.contrib.training.HParams()
hparams.add_hparam("task_name", FLAGS.task_name)
hparams.add_hparam("test_summary", FLAGS.test_summary)
hparams.add_hparam("plot_batch_size", FLAGS.plot_batch_size)
hparams.add_hparam("loss", FLAGS.loss)
hparams.add_hparam("max_iterations", FLAGS.max_iterations)
hparams.add_hparam("num_iters_D", FLAGS.num_iters_D)
hparams.add_hparam("batch_size", FLAGS.batch_size)
hparams.add_hparam("conv_deconv_baby", FLAGS.conv_deconv_baby)
hparams.add_hparam("model_family", FLAGS.model_family)
hparams.add_hparam("optimizer", FLAGS.optimizer)
hparams.add_hparam("learning_rate", FLAGS.learning_rate)
hparams.add_hparam("max_grad_norm", FLAGS.max_grad_norm)
hparams.add_hparam("solo_dim_prior_sampling", FLAGS.solo_dim_prior_sampling)
hparams.add_hparam("Z_use_label", FLAGS.Z_use_label)
hparams.add_hparam("noises_dim", FLAGS.noises_dim)
hparams.add_hparam("auto_prototype", FLAGS.auto_prototype)
hparams.add_hparam("Z_from_X", FLAGS.Z_from_X)
hparams.add_hparam("Z_sampling", FLAGS.Z_sampling)
hparams.add_hparam("Z_batch_norm", FLAGS.Z_batch_norm)
hparams.add_hparam("Z_use_gan", FLAGS.Z_use_gan)
hparams.add_hparam("Z_use_KL", FLAGS.Z_use_KL)
hparams.add_hparam("Z_soft_KL", FLAGS.Z_soft_KL)
hparams.add_hparam("X_use_gan", FLAGS.X_use_gan)
hparams.add_hparam("X_use_reconst", FLAGS.X_use_reconst)
hparams.add_hparam("XZ_joint_gan", FLAGS.XZ_joint_gan)
hparams.add_hparam("noise_transform", FLAGS.noise_transform)
hparams.add_hparam("discrete_coding_scheme", FLAGS.discrete_coding_scheme)
hparams.add_hparam("Z_discrete_K", FLAGS.Z_discrete_K)
hparams.add_hparam("Z_discrete_D", FLAGS.Z_discrete_D)
hparams.add_hparam("Z_d", FLAGS.Z_d)
hparams.add_hparam("policy_optimizer", FLAGS.policy_optimizer)
hparams.add_hparam("policy_learning_rate", FLAGS.policy_learning_rate)
hparams.add_hparam("policy_var_learning_rate", FLAGS.policy_var_learning_rate)
hparams.add_hparam("policy_control_variate", FLAGS.policy_control_variate)
hparams.add_hparam("pg_num_samples", FLAGS.pg_num_samples)
hparams.add_hparam("discrete_estimator", FLAGS.discrete_estimator)
hparams.add_hparam("vq_reg_weight", FLAGS.vq_reg_weight)
hparams.add_hparam("vq_shared_codebook", FLAGS.vq_shared_codebook)
hparams.add_hparam("vq_code_emb_merger", FLAGS.vq_code_emb_merger)
hparams.add_hparam("Z_nested_dropout", FLAGS.Z_nested_dropout)
# Dense network params, many of them will not matter if conv_deconv_baby is True
hparams.add_hparam("eps2Z_layers", FLAGS.eps2Z_layers)
hparams.add_hparam("eps2Z_hdim", FLAGS.eps2Z_hdim)
hparams.add_hparam("eps2Z_dropout", FLAGS.eps2Z_dropout)
hparams.add_hparam("eps2Z_hactv", FLAGS.eps2Z_hactv)
hparams.add_hparam("X2Zvq_layers", FLAGS.X2Zvq_layers)
hparams.add_hparam("X2Zvq_hdim", FLAGS.X2Zvq_hdim)
hparams.add_hparam("X2Zvq_dropout", FLAGS.X2Zvq_dropout)
hparams.add_hparam("X2Zvq_hactv", FLAGS.X2Zvq_hactv)
hparams.add_hparam("X2Z_layers", FLAGS.X2Z_layers)
hparams.add_hparam("X2Z_hdim", FLAGS.X2Z_hdim)
hparams.add_hparam("X2Z_dropout", FLAGS.X2Z_dropout)
hparams.add_hparam("X2Z_hactv", FLAGS.X2Z_hactv)
hparams.add_hparam("Z2X_layers", FLAGS.Z2X_layers)
hparams.add_hparam("Z2X_hdim", FLAGS.Z2X_hdim)
hparams.add_hparam("Z2X_dropout", FLAGS.Z2X_dropout)
hparams.add_hparam("Z2X_hactv", FLAGS.Z2X_hactv)
hparams.add_hparam("Z2X_xactv", FLAGS.Z2X_xactv)
hparams.add_hparam("D_layers", FLAGS.D_layers)
hparams.add_hparam("D_hdim", FLAGS.D_hdim)
hparams.add_hparam("D_dropout", FLAGS.D_dropout)
hparams.add_hparam("D_hactv", FLAGS.D_hactv)
hparams.add_hparam("weight_supervised_loss", FLAGS.weight_supervised_loss)
hparams.add_hparam("Z2Y_layers", FLAGS.Z2Y_layers)
hparams.add_hparam("Z2Y_hdim", FLAGS.Z2Y_hdim)
hparams.add_hparam("Z2Y_dropout", FLAGS.Z2Y_dropout)
hparams.add_hparam("Z2Y_hactv", FLAGS.Z2Y_hactv)
print(hparams.values())

# Get model.
train_aux_args, test_aux_args = {"is_train": True}, {"is_train": False}
if FLAGS.model_family == "regular":
  model = models.regular
elif FLAGS.model_family == "auto_encoding":
  model = models.auto_encoding
  get_model = model.get_model
elif FLAGS.model_family == "discrete_vae":
  model = models.discrete_vae
  if task_type == "synt_cloud":
    train_aux_args["idxs"] = idxs_train
    train_aux_args["num_points"] = FLAGS.num_data_points
    test_aux_args["idxs"] = idxs_test
    test_aux_args["num_points"] = FLAGS.num_data_points
elif FLAGS.model_family == "vq_vae":
  model = models.vq_vae
  if task_type == "synt_cloud":
    train_aux_args["idxs"] = idxs_train
    train_aux_args["num_points"] = FLAGS.num_data_points
    test_aux_args["idxs"] = idxs_test
    test_aux_args["num_points"] = FLAGS.num_data_points
else:
  raise ValueError("Unknown model_family {}".format(FLAGS.model_family))
auto_encoding_family = ["auto_encoding", "discrete_vae", "vq_vae"]
discrete_code_family = ["discrete_vae", "vq_vae"]  # TODO: auto_encoding may also be here.
with tf.name_scope("Train"):
  with tf.variable_scope("model", reuse=False):
    train_ops, losses_train, samples_train = model.get_model(
        data_train, labels_train, FLAGS.loss, hparams, **train_aux_args)
    train_op_join = tf.group(train_ops["D"], train_ops["G"])
with tf.name_scope("Test"):
  with tf.variable_scope("model", reuse=True):
    _, losses_test, samples_test = model.get_model(
        data_test, labels_test, FLAGS.loss, hparams, **test_aux_args)
util.get_parameter_count()

# Prepare variables for code visualization.
if FLAGS.model_family in auto_encoding_family:
  codes_train_vis = tf.get_variable(
      "codes_train_vis",
      shape=(FLAGS.num_test_samples,
             samples_train["codes"].shape.as_list()[1]),
      trainable=False)
  codes_test_vis = tf.get_variable(
      "codes_test_vis",
      shape=(FLAGS.num_test_samples,
             samples_test["codes"].shape.as_list()[1]),
      trainable=False)
  tf.add_to_collection(tf.GraphKeys.SAVEABLE_OBJECTS, codes_train_vis)
  tf.add_to_collection(tf.GraphKeys.SAVEABLE_OBJECTS, codes_test_vis)
  vis_var_names = ["codes_train_vis", "codes_test_vis"]
  codes_train_vis_input = tf.placeholder(tf.float32, codes_train_vis.shape)
  codes_train_vis_update = codes_train_vis.assign(codes_train_vis_input)
  codes_test_vis_input = tf.placeholder(tf.float32, codes_test_vis.shape)
  codes_test_vis_update = codes_test_vis.assign(codes_test_vis_input)

if not os.path.exists(FLAGS.save_sample_dir):
  os.makedirs(FLAGS.save_sample_dir)
sv = tf.train.Supervisor(logdir=FLAGS.save_dir, save_summaries_secs=5)
config_proto = tf.ConfigProto(allow_soft_placement=True)
config_proto.gpu_options.allow_growth = True
#config_proto.log_device_placement = True
sample_type = "images_%s" % FLAGS.sample_images_type
# sample_type = "codes"
with sv.managed_session(config=config_proto) as sess:
  # Local utility functions.
  def _get_codes(output_data_samples=False):
    # Obtain codes of some random train/test images, and save for visualization.
    # For continuous case, codes in (batch_size, Z_d)
    # For discrete case, codes in (batch_size, D * K)
    def _update_codes_and_return_labels(updater, input_, samples_dict, images):
      fetch_dict = {"codes": samples_dict["codes"],
                    "labels": samples_dict["labels"]}
      if images is not None:
        fetch_dict["images"] = images
        images_list = []
      codes, labels = [], []
      _num_samples_gathered = 0
      while True:
        try:
          results = sess.run(fetch_dict)
        except tf.errors.OutOfRangeError:
          break
        codes.append(results["codes"])
        labels.append(results["labels"])
        if images is not None:
          images_list.append(results["images"])
        _num_samples_gathered += codes[-1].shape[0]
        if _num_samples_gathered >= FLAGS.num_test_samples:
          break
      codes = np.concatenate(codes)[:FLAGS.num_test_samples]
      sess.run(updater, feed_dict={input_: codes})
      labels = np.concatenate(labels)[:FLAGS.num_test_samples]
      if len(labels.shape) == 2:  # one-hot labels
        labels = np.argmax(labels, 1).tolist()
      else:
        labels = labels.tolist()
      if len(codes) != len(labels):
        raise ValueError("Length of codes {} != length of labels {}".format(
            len(codes), len(labels)))
      if images is not None:
        images = np.concatenate(images_list)[:FLAGS.num_test_samples]
      return codes, labels, images
    codes_train, labels_train_vis, _ = _update_codes_and_return_labels(
        codes_train_vis_update, codes_train_vis_input, samples_train, None)
    sess.run(test_initializer)
    if output_data_samples:
      images_in = data_test
    else:
      images_in = None
    codes_test, labels_test_vis, images_test = _update_codes_and_return_labels(
        codes_test_vis_update, codes_test_vis_input, samples_test, images_in)
    return codes_train, labels_train_vis, codes_test, labels_test_vis, images_test

  # Training.
  last_timestamp = time.time()
  eval_duration = 0.0
  gen_loss, dis_loss = [], []
  for it in range(FLAGS.max_iterations + 1):
    if not train_ops["D"].name.startswith("constant_noop"):
      for _ in range(FLAGS.num_iters_D):
        sess.run(train_ops["D"])
    # fetch_dict = {"losses_train": losses_train, "train_op_G": train_ops["G"]}
    fetch_dict = {"losses_train": losses_train, "train_op": train_op_join}
    if it % FLAGS.verbose_iters == 0:
        fetch_dict[sample_type] = samples_train[sample_type]
        if hparams.solo_dim_prior_sampling:
          fetch_dict["Z_random"] = samples_train["Z_random"]
    results = sess.run(fetch_dict)
    gen_loss.append(results["losses_train"]["gen_loss"])
    dis_loss.append(results["losses_train"]["dis_loss"])
    if it % FLAGS.verbose_iters == 0:
      # Stats.
      curt_timestamp = time.time()
      print("Iter {} {:.2f} (train sec) {:.2f} (last eval sec)".format(
          it, curt_timestamp - last_timestamp, eval_duration))
      print("\tgen_loss {:.4f} dis_loss {:.4f}".format(
          np.mean(gen_loss), np.mean(dis_loss)))
      gen_loss, dis_loss = [], []
      if FLAGS.do_plot_data:
        # Plot images.
        def plot_it(results, do_solo=False, tag=""):
          solo_dim = ""
          if task_type == "images":
            if FLAGS.sample_images_type == "random":
              if results[sample_type].shape[0] != hparams.plot_batch_size and (
                  results[sample_type].shape[0] != 2 * hparams.plot_batch_size):
                raise ValueError("random sampled images have wrong batch_size "
                                 "{}".format(results[sample_type].shape))
              if (not do_solo) and hparams.solo_dim_prior_sampling:
                # smart plot random samples even when solo_dim_prior_sampling=True
                images_to_plot = results[sample_type][hparams.plot_batch_size:]
              else:
                images_to_plot = results[sample_type][:hparams.plot_batch_size]
                if do_solo:
                  Z_random = results["Z_random"][:hparams.plot_batch_size]
                  Z_random_std = np.std(Z_random, 0)
                  if (Z_random_std > 1e-3).sum() != 1:
                    raise ValueError("Only 1 dim should differ when use solo.")
                  solo_dim = "-sdim{}".format(np.argmax(Z_random_std))
            else:
              idxs = np.random.randint(
                  0, results[sample_type].shape[0], hparams.plot_batch_size)
              images_to_plot = results[sample_type][idxs]
            filename = os.path.join(FLAGS.save_sample_dir, "%d" % it)
            data.plot_images(
                images_to_plot, rgb=plog_rgb,
                filename=filename + solo_dim + tag)
            fetch_dict.pop(sample_type)
          elif task_type == "synt_cloud":
            if FLAGS.data_dim <= 3:
              samples = results[sample_type]
            else:  # Projection to 2d space.
              samples = PCA(n_components=2).fit_transform(results[sample_type])
            filename = os.path.join(FLAGS.save_sample_dir, "%d" % it)
            data.plot(samples, filename=filename + solo_dim + tag)
        plot_it(results)
        if hparams.solo_dim_prior_sampling:  # Plot more.
          for j in range(10):
            fetch_dict = {sample_type: samples_train[sample_type],
                          "Z_random": samples_train["Z_random"]}
            results = sess.run(fetch_dict)
            plot_it(results, do_solo=True, tag="-%d"%j)

      do_latent_eval = FLAGS.do_evaluation and (
          FLAGS.model_family in auto_encoding_family and (
          task_type != "features" or FLAGS.use_raw_data_distance_for_eval > 0))
      if do_latent_eval:
        # Prepare for code evaluations.
        (codes_train, labels_train_vis, codes_test,
            labels_test_vis, images_test) = _get_codes(
            output_data_samples=FLAGS.plot_nn_cases)
        if FLAGS.model_family in discrete_code_family:
          # Reshape the code from one-hot to integer.
          codes_train = codes_train.reshape(
              (-1, hparams.Z_discrete_D, hparams.Z_discrete_K))
          codes_train = np.argmax(codes_train, -1)
          codes_test = codes_test.reshape(
              (-1, hparams.Z_discrete_D, hparams.Z_discrete_K))
          codes_test = np.argmax(codes_test, -1)
        elif not str(codes_train.dtype).startswith("int"):
          # Kmeans evaluation (make more sense for continuous code).
          _res_tr = metrics.kmeans_eval(codes_train, labels_train_vis)
          _res_te = metrics.kmeans_eval(codes_test, labels_test_vis)
          print("\tTrain cluster nmi {:.4}, adj_nmi {:.4}, adj_rand {:.4}".format(
              _res_tr["nmi"], _res_tr["adjusted_nmi"], _res_tr["adjusted_rand"]))
          print("\tTest cluster nmi {:.4}, adj_nmi {:.4}, adj_rand {:.4}".format(
              _res_te["nmi"], _res_te["adjusted_nmi"], _res_te["adjusted_rand"]))
        # KNN evaluation.
        assert len(codes_train) == len(labels_train_vis)
        assert len(codes_test) == len(labels_test_vis)
        def _run_and_report(codes_train, codes_test,
                            eval_func, labels_train, labels_test, tag=""):
          _res_tr = eval_func(codes_train, labels_train, ks, return_cases=None)
          _res_te = eval_func(codes_test, labels_test, ks)
          _precisions_tr = " ".join(
              ["{:.4f}".format(_res_tr["precisions"][i]) for i in range(len(ks))])
          _recalls_tr = " ".join(
              ["{:.4f}".format(_res_tr["recalls"][i]) for i in range(len(ks))])
          _precisions_te = " ".join(
              ["{:.4f}".format(_res_te["precisions"][i]) for i in range(len(ks))])
          _recalls_te = " ".join(
              ["{:.4f}".format(_res_te["recalls"][i]) for i in range(len(ks))])
          print("\tTrain{} KNN precisions: {} (last_upbound: {:.4f})".format(
              tag, _precisions_tr, _res_tr["precisions"][-1]))
          print("\tTrain{} KNN recalls:    {} (last_upbound: {:.4f})".format(
              tag, _recalls_tr, _res_tr["recalls"][-1]))
          print("\tTest{}  KNN precisions: {} (last_upbound: {:.4f})".format(
              tag, _precisions_te, _res_te["precisions"][-1]))
          print("\tTest{}  KNN recalls:    {} (last_upbound: {:.4f})".format(
              tag, _recalls_te, _res_te["recalls"][-1]))
          return _res_te["nn_cases"]
        if FLAGS.hierarchical_and_flat_eval:
          _flat_eval, _hier_eval = True, True
        elif FLAGS.hierarchical_eval:
          _flat_eval, _hier_eval = False, True
        else:
          _flat_eval, _hier_eval = True, False
        if FLAGS.use_raw_data_distance_for_eval > 0:
          relevance_k = FLAGS.use_raw_data_distance_for_eval
          labels_train = metrics.find_knns(codes_train, relevance_k)  # DEBUG: should be images_train.
          labels_test = metrics.find_knns(images_test, relevance_k)
        else:
          labels_train = labels_train_vis
          labels_test = labels_test_vis
        nn_cases_f = nn_cases_h = nn_cases_hn = None
        ks = [1, 10, 100, 1000] # TODO: hparams
        if _flat_eval:
          eval_func = metrics.knn_eval_by_labels
          nn_cases_f = _run_and_report(
              codes_train, codes_test, eval_func,
              labels_train, labels_test, tag="-f")
        if _hier_eval:
          code_reverse_order = True
          eval_func = partial(metrics.hknn_eval_by_labels,
                              reverse_order=code_reverse_order)
          nn_cases_h = _run_and_report(
              codes_train, codes_test, eval_func,
              labels_train, labels_test, tag="-h")
          eval_func = partial(metrics.hknn_eval_by_labels, # DEBUG, normal direction.
                              reverse_order=False)
          nn_cases_hn = _run_and_report(
              codes_train, codes_test, eval_func,
              labels_train, labels_test, tag="-hn")
        # Plot images of near neighbor cases.
        if task_type == "images" and images_test is not None:
          def plot_data_cases(nn_cases, filename):
            num_q, num_nb_p1 = nn_cases.shape
            samples_d = images_test[nn_cases].reshape(
                (num_q * num_nb_p1, images_test.shape[-1]))
            data.plot_images(samples_d, rgb=plog_rgb,
                             fsize=nn_cases.shape, filename=filename)
          if nn_cases_f is not None:
            filename = os.path.join(FLAGS.save_sample_dir, "cases-%d-f" % it)
            plot_data_cases(nn_cases_f, filename)
          if nn_cases_h is not None:
            filename = os.path.join(FLAGS.save_sample_dir, "cases-%d-h" % it)
            plot_data_cases(nn_cases_h, filename)
          if nn_cases_hn is not None:
            filename = os.path.join(FLAGS.save_sample_dir, "cases-%d-hb" % it)
            plot_data_cases(nn_cases_hn, filename)

      last_timestamp = time.time()
      eval_duration = last_timestamp - curt_timestamp

  # Final output.
  if FLAGS.model_family in auto_encoding_family:
    # Save embedding meta.
    codes_train, labels_train_vis, codes_test, labels_test_vis, _ = _get_codes()
    vis_labels_list = [labels_train_vis, labels_test_vis]
    util.save_emb_visualize_meta(
            FLAGS.save_dir, vis_var_names, vis_labels_list)
    if FLAGS.code_label_filename is not None:
      # Transform the one-hot code to code string.
      if codes_train.dtype.name.startswith("int"):
        D1 = codes_train[0].sum()
        D2 = codes_test[1].sum()
        if D1 != D2:
          tf.logging.warn("The dimension does not match in discrete code.")
        codes_train = np.argmax(
            codes_train.reshape((codes_train.shape[0], int(D1), -1)), -1)
        codes_test = np.argmax(
            codes_test.reshape((codes_test.shape[0], int(D2), -1)), -1)
      path = os.path.join(FLAGS.save_dir, FLAGS.code_label_filename)
      with open(path + "_train.txt", "w") as fp:
        for label, code in sorted(zip(labels_train_vis, codes_train.tolist())):
          fp.write("{}\t{}\n".format(label, "-".join(map(str, code))))
      with open(path + "_test.txt", "w") as fp:
        for label, code in sorted(zip(labels_test_vis, codes_test.tolist())):
          fp.write("{}\t{}\n".format(label, "-".join(map(str, code))))

  # Save final model at the end. Avoids non-updates in final output section.
  sv.saver.save(sess,
                os.path.join(FLAGS.save_dir, "model"),
                global_step=sv.global_step)

  # Save all test codes.
  if task_type == "features":
    print("start saving test codes.")
    filename = os.path.join(FLAGS.save_dir, "test_codes.txt")
    util.save_feature_codes(
        sess, samples_test["codes"], filename, hparams, test_initializer)
