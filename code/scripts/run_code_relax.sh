#!/bin/bash

. ./code_mnist.sh
cd ..
gpu=$1
model_family=discrete_vae
discrete_estimator=relax

noises_dim=$Z_discrete_D
Z_sampling=1
X_use_reconst=1
Z_use_KL=0.
Z_soft_KL=True

policy_learning_rate=$learning_rate
policy_control_variate=False
policy_var_learning_rate=1e-3
#policy_learning_rate=0
#policy_var_learning_rate=0


CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python main.py --task_name=$task_name --data_dir=$data_dir --save_dir=$save_dir --save_sample_dir=$save_sample_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --Z_discrete_D=$Z_discrete_D --noises_dim=$noises_dim --X_use_reconst=$X_use_reconst --Z_use_KL=$Z_use_KL --Z_sampling=$Z_sampling --discrete_estimator=$discrete_estimator --Z_discrete_K=$Z_discrete_K --Z2X_xactv=$Z2X_xactv --Z_soft_KL=$Z_soft_KL --learning_rate=$learning_rate --policy_learning_rate=$policy_learning_rate --policy_var_learning_rate=$policy_var_learning_rate --policy_control_variate=$policy_control_variate --hierarchical_eval=$hierarchical_eval --hierarchical_and_flat_eval=$hierarchical_and_flat_eval --discrete_coding_scheme=$discrete_coding_scheme --num_test_samples=$num_test_samples --solo_dim_prior_sampling=$solo_dim_prior_sampling --weight_supervised_loss=$weight_supervised_loss --use_raw_data_distance_for_eval=$use_raw_data_distance_for_eval --conv_deconv_baby=$conv_deconv_baby --Z_nested_dropout=$Z_nested_dropout 2>&1| tee $save_dir/log


