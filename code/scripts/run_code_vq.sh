#!/bin/bash

. ./code_mnist.sh
cd ..
gpu=$1
model_family=vq_vae
discrete_coding_scheme=flat  # Only support flat for now.

noises_dim=$Z_discrete_D
Z_d=100
vq_reg_weight=1
vq_shared_codebook=False
vq_code_emb_merger=concat
Z_sampling=0


CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python main.py --task_name=$task_name --data_dir=$data_dir --save_dir=$save_dir --save_sample_dir=$save_sample_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --Z_sampling=$Z_sampling --Z_discrete_K=$Z_discrete_K --Z_discrete_D=$Z_discrete_D --noises_dim=$noises_dim --Z_d=$Z_d --vq_reg_weight=$vq_reg_weight --vq_shared_codebook=$vq_shared_codebook --vq_code_emb_merger=$vq_code_emb_merger --Z2X_xactv=$Z2X_xactv --hierarchical_eval=$hierarchical_eval --hierarchical_and_flat_eval=$hierarchical_and_flat_eval --learning_rate=$learning_rate --discrete_coding_scheme=$discrete_coding_scheme --num_test_samples=$num_test_samples --solo_dim_prior_sampling=$solo_dim_prior_sampling --weight_supervised_loss=$weight_supervised_loss --use_raw_data_distance_for_eval=$use_raw_data_distance_for_eval --conv_deconv_baby=$conv_deconv_baby --Z_nested_dropout=$Z_nested_dropout 2>&1| tee $save_dir/log


