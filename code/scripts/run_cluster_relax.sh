#!/bin/bash

. ./cluster_task.sh
cd ..
gpu=$1

model_family=discrete_vae
discrete_estimator=relax
max_iterations=100000

noises_dim=1
Z_discrete_D=$noises_dim
Z_discrete_K=$num_clusters
#Z_sampling=1
X_use_reconst=1
Z_use_KL=0
Z_soft_KL=False
learning_rate=1e-3

CUDA_VISIBLE_DEVICES=$gpu python main.py --task_name=$task_name --data_gen_seed=$data_gen_seed --data_dir=$data_dir --save_sample_dir=$save_sample_dir --data_dim=$data_dim --num_clusters=$num_clusters --cluster_std=$cluster_std --save_dir=$save_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --discrete_estimator=$discrete_estimator --noises_dim=$noises_dim --Z_discrete_D=$Z_discrete_D --Z_discrete_K=$Z_discrete_K --X_use_reconst=$X_use_reconst --Z_use_KL=$Z_use_KL --Z_soft_KL=$Z_soft_KL --learning_rate=$learning_rate --num_data_points=$num_data_points --num_test_samples=$num_test_samples
