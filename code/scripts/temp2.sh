#!/bin/bash

. ./tmp_code_mnist.sh
cd ..
gpu=$1
model_family=discrete_vae
discrete_estimator=gumbel

noises_dim=$Z_discrete_D
Z_sampling=1
X_use_reconst=1
Z_use_KL=1
Z_soft_KL=True

save_dir_base=$save_dir
for discrete_coding_scheme in args_dec ; do
for Z_nested_dropout in True; do
Z_discrete_K=10
Z_discrete_D=10
noises_dim=$Z_discrete_D
save_dir=$save_dir_base\_$discrete_coding_scheme\_$Z_discrete_K\_$Z_discrete_D\_$Z_nested_dropout
save_sample_dir=$save_dir/samples
if [ ! -e $save_dir ]; then
    mkdir $save_dir
fi
echo $save_dir
CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python main.py --task_name=$task_name --data_dir=$data_dir --save_dir=$save_dir --save_sample_dir=$save_sample_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --Z_discrete_D=$Z_discrete_D --noises_dim=$noises_dim --X_use_reconst=$X_use_reconst --Z_use_KL=$Z_use_KL --Z_sampling=$Z_sampling --discrete_estimator=$discrete_estimator --Z_discrete_K=$Z_discrete_K --Z2X_xactv=$Z2X_xactv --Z_soft_KL=$Z_soft_KL --hierarchical_eval=$hierarchical_eval --hierarchical_and_flat_eval=$hierarchical_and_flat_eval --discrete_coding_scheme=$discrete_coding_scheme --num_test_samples=$num_test_samples --solo_dim_prior_sampling=$solo_dim_prior_sampling --weight_supervised_loss=$weight_supervised_loss --learning_rate=$learning_rate --use_raw_data_distance_for_eval=$use_raw_data_distance_for_eval --conv_deconv_baby=$conv_deconv_baby --Z_nested_dropout=$Z_nested_dropout 2>&1| tee $save_dir/log
done
done
