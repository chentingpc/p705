#!/bin/bash

. ./code_mnist.sh
cd ..
gpu=$1
model_family=auto_encoding
auto_prototype=ali
#auto_prototype=ae_gan  # DEBUG
#auto_prototype=ae_ali

#loss=logistic
#loss=ls
#num_iters_D=1
loss=imwass
num_iters_D=4

# Following not effective with auto_prototype
Z_sampling=1
X_use_reconst=0
Z_use_KL=0
Z_batch_norm=False


CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python main.py --task_name=$task_name --data_dir=$data_dir --save_dir=$save_dir --save_sample_dir=$save_sample_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --loss=$loss --num_iters_D=$num_iters_D --noises_dim=$noises_dim --Z_discrete_K=$Z_discrete_K --Z_discrete_D=$Z_discrete_D --X_use_reconst=$X_use_reconst --Z_use_KL=$Z_use_KL --Z_sampling=$Z_sampling --Z_batch_norm=$Z_batch_norm --auto_prototype=$auto_prototype --Z2X_xactv=$Z2X_xactv --hierarchical_eval=$hierarchical_eval --hierarchical_and_flat_eval=$hierarchical_and_flat_eval --discrete_coding_scheme=$discrete_coding_scheme --num_test_samples=$num_test_samples --solo_dim_prior_sampling=$solo_dim_prior_sampling --weight_supervised_loss=$weight_supervised_loss --learning_rate=$learning_rate --use_raw_data_distance_for_eval=$use_raw_data_distance_for_eval --conv_deconv_baby=$conv_deconv_baby --Z_nested_dropout=$Z_nested_dropout 2>&1| tee $save_dir/log


