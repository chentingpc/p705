#!/bin/bash

#task_name=mnist
task_name=clusters
data_dir=~/dbase/image/mnist/tf_record/
save_root=~/pbase/x/p705/results/temp/
save_dir=$save_root/$(date +%H%M%S)
save_sample_dir=$save_dir/samples
num_data_points=10000
#num_data_points=100
num_test_samples=2000
data_dim=2
num_clusters=16
cluster_std=0.1
#num_clusters=5
#cluster_std=0.5
data_gen_seed=100
sample_images_type=reconst


if [ ! -e $save_sample_dir ]; then
	mkdir -p $save_sample_dir
fi


