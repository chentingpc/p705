#!/bin/bash
. ./cluster_task.sh
cd ..
gpu=$1

model_family=vq_vae
max_iterations=100000

Z_discrete_K=$num_clusters
Z_discrete_D=1
Z_d=2
vq_reg_weight=1
vq_shared_codebook=True
vq_code_emb_merger=average

CUDA_VISIBLE_DEVICES=$gpu python main.py --task_name=$task_name --data_gen_seed=$data_gen_seed --data_dir=$data_dir --save_sample_dir=$save_sample_dir --data_dim=$data_dim --num_clusters=$num_clusters --cluster_std=$cluster_std --save_dir=$save_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --Z_discrete_K=$Z_discrete_K --Z_discrete_D=$Z_discrete_D --Z_d=$Z_d --vq_reg_weight=$vq_reg_weight --vq_shared_codebook=$vq_shared_codebook --vq_code_emb_merger=$vq_code_emb_merger --num_data_points=$num_data_points --num_test_samples=$num_test_samples


