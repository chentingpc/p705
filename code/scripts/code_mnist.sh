#!/bin/bash
task_name=mnist
#task_name=cifar10
#task_name=sift1m
#task_name=gist1m
if [[ "$task_name" =~ ^(gist1m|sift1m|sift10k)$ ]]; then
    data_dir=~/dbase/image/ann/$task_name/
    Z2X_xactv=linear
else
    data_dir=~/dbase/image/$task_name/
    Z2X_xactv=sigmoid
fi
num_test_samples=10000
save_root=~/pbase/x/p705/results/temp/
save_dir=$save_root/$(date +%d-%H%M%S)
save_sample_dir=$save_dir/samples
sample_images_type=reconst
#sample_images_type=random
if [ ! -e $save_dir ]; then
    mkdir $save_dir
fi
echo $save_dir

Z_discrete_K=2
Z_discrete_D=10
noises_dim=5
max_iterations=5000
#max_iterations=100000  # DEBUG
weight_supervised_loss=00.
conv_deconv_baby=False
Z_nested_dropout=False
learning_rate=1e-3
discrete_coding_scheme=flat
#discrete_coding_scheme=vlae
#discrete_coding_scheme=args
discrete_coding_scheme=args_dec
#discrete_coding_scheme=hyper
hierarchical_eval=False
hierarchical_and_flat_eval=True
solo_dim_prior_sampling=False
use_raw_data_distance_for_eval=0
