#!/bin/bash
. ./cluster_task.sh
cd ..
gpu=$1

model_family=discrete_vae
discrete_estimator=policy
max_iterations=100000

noises_dim=1
Z_discrete_D=$noises_dim
Z_discrete_K=$num_clusters
X_use_reconst=1
Z_use_KL=1
# pg_num_samples=-4
pg_num_samples=4

CUDA_VISIBLE_DEVICES=$gpu python main.py --task_name=$task_name --data_gen_seed=$data_gen_seed --data_dir=$data_dir --save_sample_dir=$save_sample_dir --data_dim=$data_dim --num_clusters=$num_clusters --cluster_std=$cluster_std --save_dir=$save_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --discrete_estimator=$discrete_estimator --noises_dim=$noises_dim --Z_discrete_D=$Z_discrete_D --Z_discrete_K=$Z_discrete_K --X_use_reconst=$X_use_reconst --Z_use_KL=$Z_use_KL --pg_num_samples=$pg_num_samples --num_data_points=$num_data_points --num_test_samples=$num_test_samples


