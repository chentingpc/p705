#!/bin/bash
cd ..

gpu=$1
task_name=mnist
data_dir=~/dbase/image/mnist/tf_record/
save_root=~/pbase/x/p705/results/temp/
save_dir=$save_root/$(date +%H%M%S)
#save_dir=/tmp/tf/gan/$(date +%H%M%S)
max_iterations=100000
#max_iterations=3000
save_sample_dir=$save_dir/samples
#model_family=regular
#model_family=auto_encoding
model_family=vq_vae
#model_family=discrete_vae
#sample_images_type=reconst
sample_images_type=random
num_iters_D=4
loss=logistic
loss=ls
loss=imwass

CUDA_VISIBLE_DEVICES=$gpu python main.py --task_name=$task_name --data_dir=$data_dir --save_dir=$save_dir --save_sample_dir=$save_sample_dir --max_iterations=$max_iterations --model_family=$model_family --sample_images_type=$sample_images_type --loss=$loss --num_iters_D=$num_iters_D --num_test_samples=$num_test_samples
