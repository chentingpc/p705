from functools import partial
import tensorflow as tf


def continuous_prior_sampling(
    batch_size, dim, solo_dim=False, smart_solo=True, hparams=None):
  """Return prior samples.

  Args:
    solo_dim: sample only 1 out-of-D dim while fixing the other randomly.
    smart_solo: attach normal prior samples to solo_dim sampled ones.

  Returns:
   Prior samples shape of (batch_size, D, K), or (batch_size*2, D, K) when
    solo_dim and smart_solo are both True.
  """
  random_function = tf.random_normal  # tf.random_uniform

  def random_prior():
    return random_function([batch_size, dim])

  if solo_dim:
    epsilon_1 = random_function([1, dim - 1])
    epsilon_2 = random_function([batch_size, 1])
    epsilon = tf.concat([tf.tile(epsilon_1, [batch_size, 1]), epsilon_2], 1)
    epsilon = tf.transpose(tf.random_shuffle(tf.transpose(epsilon)))

    if smart_solo:
      epsilon_rnd = random_prior()
      epsilon = tf.concat([epsilon, epsilon_rnd], 0)
  else:
    epsilon = random_prior()

  return epsilon


def discrete_prior_sampling(
    batch_size, D, K,
    solo_dim=False, smart_solo=True, one_hot=True, hparams=None):
  """Return prior samples.

  Args:
    solo_dim: sample only 1 out-of-D dim while fixing the other randomly.
    smart_solo: attach normal prior samples to solo_dim sampled ones.

  Returns:
   Prior samples shape of (batch_size, D, K), or (batch_size*2, D, K) when
    solo_dim and smart_solo are both True.
  """
  def random_prior():
    return tf.random_uniform([batch_size, D],
                             minval=0, maxval=K, dtype=tf.int32)

  if solo_dim:
    # Fix D-1 dims, only varies on 1 dimension for samples of batch_size.
    B_prior_1 = tf.random_uniform([1, D - 1],
                                  minval=0, maxval=K, dtype=tf.int32)
    B_prior_2 = tf.random_uniform([batch_size, 1],
                                  minval=0, maxval=K, dtype=tf.int32)
    B_prior = tf.concat([tf.tile(B_prior_1, [batch_size, 1]), B_prior_2], 1)
    B_prior = tf.transpose(tf.random_shuffle(tf.transpose(B_prior)))

    if smart_solo:
      B_prior_rnd = random_prior()
      B_prior = tf.concat([B_prior, B_prior_rnd], 0)
  else:
    B_prior = random_prior()

  if one_hot:
    B_prior = tf.cast(tf.one_hot(B_prior, K), tf.int32)

  return B_prior


def nested_dropout(Z, hparams, is_train):
  """Nested dropout Z of shape (batch_size, D, K)."""
  batch_size = tf.shape(Z)[0]
  Z_discrete_D = hparams.Z_discrete_D

  if is_train:
    idxs = tf.tile(tf.expand_dims(tf.range(0, Z_discrete_D), 0),
                   [batch_size, 1])
    rnds = tf.random_uniform(
        [batch_size, 1], minval=0, maxval=Z_discrete_D, dtype=tf.int32)
    mask = tf.cast(tf.expand_dims(tf.less_equal(rnds, idxs), -1), tf.float32)  # (batch_size, D, 1)
    retain_rate = tf.cast(tf.range(1, Z_discrete_D + 1), tf.float32)
    retain_rate = tf.reshape(retain_rate, [1, Z_discrete_D, 1]) / Z_discrete_D
    Z *= mask
    Z /= retain_rate
  return Z


def progressive_dropout(Z, hparams, is_train):
  """Progressive dropout Z of shape (batch_size, D, K).

  Differences with nested_dropout: (1) single dropout pattern for all examples
  in a single batch, (2) only BP through lowest single non-dropout dimension.
  """
  batch_size = tf.shape(Z)[0]
  Z_discrete_D = hparams.Z_discrete_D

  if is_train:
    # Nested dropout mask.
    idxs = tf.expand_dims(tf.range(0, Z_discrete_D), 0)
    rnds = tf.random_uniform(
        [1, 1], minval=0, maxval=Z_discrete_D, dtype=tf.int32)
    mask = tf.cast(tf.expand_dims(tf.less_equal(rnds, idxs), -1), tf.float32)  # (1, D, 1)
    retain_rate = tf.cast(tf.range(1, Z_discrete_D + 1), tf.float32)
    retain_rate = tf.reshape(retain_rate, [1, Z_discrete_D, 1]) / Z_discrete_D

    # Only BP through lowest single non-dropout dimension.
    Zs = tf.unstack(Z, num=Z_discrete_D, axis=1)  # D of (batch_size, K)
    active_Z = lambda d: Zs[d]
    inactive_Z = lambda d: tf.stop_gradient(Zs[d])
    mask_bp = tf.equal(rnds, idxs)  # (1, D)
    Zs_new = [tf.cond(mask_bp[0, d],
                      partial(active_Z, d=d),
                      partial(inactive_Z, d=d)) for d in range(Z_discrete_D)]
    Z_new = tf.stack(Zs_new, axis=1)
    Z = Z_new

    Z *= mask
    Z /= retain_rate

  return Z


def nested_dropout_onedim(Z, dim, hparams, is_train):
  """Nested dropout on one of D dims of (batch_size, D, K).

  Args:
    Z: (batch_size, K), a slice of mid the dimensions.
    dim: the dimension in [0, Z_discrete_D-1) to dropout.
  """
  batch_size = tf.shape(Z)[0]
  Z_discrete_D = hparams.Z_discrete_D

  if is_train:
    rnds = tf.random_uniform(
        [batch_size, 1], minval=0, maxval=Z_discrete_D, dtype=tf.int32)
    mask = tf.cast(tf.less_equal(rnds, dim), tf.float32)  # (batch_size, 1)
    retain_rate = tf.cast(dim + 1, tf.float32) / Z_discrete_D
    Z *= mask
    Z /= retain_rate

  return Z


def progressive_dropout_onedim(Z, dim, hparams, is_train):
  """Progressive dropout on one of D dims of (batch_size, D, K).

  Differences with nested_dropout: (1) single dropout pattern for all examples
  in a single batch, (2) only BP through lowest single non-dropout dimension.

  Args:
    Z: (batch_size, K), a slice of mid the dimensions.
    dim: the dimension in [0, Z_discrete_D-1) to dropout.
  """
  batch_size = tf.shape(Z)[0]
  Z_discrete_D = hparams.Z_discrete_D

  if is_train:
    # Nested dropout mask.
    rnds = tf.random_uniform(
        [1, 1], minval=0, maxval=Z_discrete_D, dtype=tf.int32)
    mask = tf.cast(tf.less_equal(rnds, dim), tf.float32)  # (1, 1)
    retain_rate = tf.cast(dim + 1, tf.float32) / Z_discrete_D

    # Only BP through lowest single non-dropout dimension.
    active_Z = lambda : Z
    inactive_Z = lambda : tf.stop_gradient(Z)
    mask_bp = tf.equal(rnds, dim)  # (1, 1)
    Z_new = tf.cond(mask_bp[0, 0], active_Z, inactive_Z)
    Z = Z_new

    Z *= mask
    Z /= retain_rate

  return Z