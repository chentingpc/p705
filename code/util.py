from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
from functools import partial
import tensorflow as tf

from tensorflow.core.framework import variable_pb2
from tensorflow.core.protobuf import rewriter_config_pb2
from tensorflow.contrib.tensorboard.plugins import projector

FLAGS = tf.flags.FLAGS

eps_micro = 1e-15  # tf.float32 sensible.
eps_tiny = 1e-10   # tf.float32 sensible.
eps_small = 3e-8   # tf.float16 sensible.


def safer_log(x, eps=eps_micro):
  """Avoid nan when x is zero by adding small eps.
  
  Note that if x.dtype=tf.float16, \forall eps, eps < 3e-8, is equal to zero.
  """
  return tf.log(x + eps)


def get_activation(name):
  """Returns activation function given name."""
  name = name.lower()
  if name == "relu":
    return tf.nn.relu
  elif name == "sigmoid":
    return tf.nn.sigmoid
  elif name == "tanh":
    return tf.nn.sigmoid
  elif name == "elu":
    return tf.nn.elu
  elif name == "leaky_relu":
    return tf.keras.layers.LeakyReLU(alpha=0.2)
  elif name == "linear":
    return lambda x: x
  else:
    raise ValueError("Unknown activation name {}".format(name))
  return name


def get_optimizer(name):
  name = name.lower()
  if name == "sgd":
    optimizer = tf.train.GradientDescentOptimizer
  elif name == "adam":
    # optimizer = tf.train.AdamOptimizer  # TODO: put it to hparams.
    optimizer = partial(tf.train.AdamOptimizer, beta1=0.5, beta2=0.9)
  elif name == "lazy_adam":
    optimizer = tf.contrib.opt.LazyAdamOptimizer
  elif name == "adagrad":
    optimizer = tf.train.AdagradOptimizer
  elif name == "rmsprop":
    optimizer = tf.train.RMSPropOptimizer
  else:
    raise ValueError("Unknown optimizer name {}.".format(name))

  return optimizer


def get_parameter_count(excludings=None, display_count=True):
  trainables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
  count = 0
  for var in trainables:
    ignored = False
    if excludings is not None:
      for excluding in excludings:
        if var.name.find(excluding) >= 0:
          ignored = True
          break
    if ignored:
      continue
    if var.shape == tf.TensorShape(None):
      tf.logging.warn("var {} has unknown shape and it is not counted.".format(
          var.name))
      continue
    if var.shape.as_list() == []:
      count_ = 1
    else:
      count_ = reduce(lambda x, y: x * y, var.shape.as_list())
    count += count_
    if display_count:
      print("{0:80} {1}".format(
          var.name, count_))
  return count


def save_emb_visualize_meta(save_path,
                            emb_var_names,
                            label_lists,
                            metadata_names=None):
  """Save meta information about the embedding visualization in tensorboard.

  Args:
    save_path: a `string` specifying the save location.
    emb_var_names: a `list` containing variable names.
    label_lists: a `list` of lists of labels, each label list corresponds to an
      emb_var_name.
    metadata_names: a `list` of file names for metadata, if not specify, will
      use emb_var_names.
  """
  if not isinstance(emb_var_names, list):
    raise ValueError("emb_var_names must be a list of var names.")
  if not isinstance(label_lists, list) or not isinstance(label_lists[0], list):
    raise ValueError("label_lists must be a list of label lists.")

  if metadata_names is None:
    metadata_names = emb_var_names

  config = projector.ProjectorConfig()
  for emb_var_name, metadata_name, labels in zip(
      emb_var_names, metadata_names, label_lists):
    filename = "metadata-{}.tsv".format(metadata_name)
    metadata_path = os.path.join(save_path, filename)
    with open(metadata_path, "w") as fp:
      for label in labels:
        fp.write("{}\n".format(label))
    embedding = config.embeddings.add()
    embedding.tensor_name = emb_var_name
    embedding.metadata_path = metadata_path
  summary_writer = tf.summary.FileWriter(save_path)
  projector.visualize_embeddings(summary_writer, config)


def save_feature_codes(sess, codes_tensor, filename, hparams, initializer=None):
  """Get codes, only stop when input is exhausted; and save."""
  if initializer is not None:
    sess.run(initializer)
  codes = []
  while True:
    try:
      code = sess.run(codes_tensor)
    except tf.errors.OutOfRangeError:
      break
    codes.append(code)
  codes = np.concatenate(codes)
  codes = codes.reshape((-1, hparams.Z_discrete_D, hparams.Z_discrete_K))
  codes = np.argmax(codes, -1)
  # Save.
  with open(filename, "w") as fp:
    for code in codes.tolist():
      fp.write("{}\n".format("-".join(map(str, code))))