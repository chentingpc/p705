import tensorflow as tf
import gumbel as gb
import sampling
import nets
import util
from hcode_vlae import X2Z as hcode_vlae_X2Z
from hcode_args import X2Z as hcode_args_X2Z
gumbel_softmax = gb.gumbel_softmax
safer_log = util.safer_log

struct_solution = 1  # DEBUG. 1: nothing, 2: x2z_bn, z2x_residual, 3: x2z_bn, x2z_residual, z2x_residual


def X2Z(X, pgor, hparams, is_train):
  return hcode_vlae_X2Z(X, pgor, hparams, is_train)
  # return hcode_args_X2Z(X, pgor, hparams, is_train)


def Z2X(Z, X, hparams, is_train):
  """Returns reconstructed data/images.

  Args:
    Z: latent posterior samples. (batch_size, D, K) if pgor is None, otherwise 
      (pg_num_samples, batch_size, D, K). It is also Okay to flatten D*K.
    X: targe/original X. (batch_size, X_shape)  if pgor is None, otherwise
      (pg_num_samples * batch_size, X_shape)

  Returns:
    X_reconst: (batch_size, X_shape) if pgor is None, otherwise
       (pg_num_samples * batch_size, X_shape)
    X_loss_batch: (batch_size, ) if pgor is None, otherwise
      (pg_num_samples * batch_size, )
  """
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  h_dim = hparams.Z2X_hdim
  h_dropout = hparams.Z2X_dropout
  h_actv = util.get_activation(hparams.Z2X_hactv)
  x_actv = util.get_activation(hparams.Z2X_xactv)
  X_shape = X.shape.as_list()[-1]
  code_h_dim = 32  # DEBUG

  with tf.variable_scope("G_X", reuse=not is_train):
    if len(Z.shape.as_list()) != 3:  # Skip if already shaped.
      Z = tf.reshape(Z, [-1, Z_discrete_D, Z_discrete_K])
    if hparams.Z_nested_dropout:
      Z = sampling.nested_dropout(Z, hparams, is_train)

    def hyper_net(codes, code_h_dim, out_h_dim):
      """(bs, D, K) => D of (bs, D, h_dim, h_dim) and D of (bs, h_dim, 1)"""
      with tf.variable_scope("hyper_net"):
        CodeEmbs = tf.get_variable("CodeEmbs",
                                   [1, Z_discrete_D, code_h_dim, Z_discrete_K],
                                   dtype=tf.float32)
        code_embs = tf.matmul(tf.tile(CodeEmbs, [tf.shape(codes)[0], 1, 1, 1]),
                              tf.expand_dims(codes, -1))
        code_embs = tf.reshape(code_embs, [-1, Z_discrete_D, code_h_dim])
        weights = tf.layers.dense(code_embs, h_dim**2)  # (bs, D, h_dim^2)
        weights = tf.reshape(weights, [-1, Z_discrete_D, h_dim, h_dim])
        weights = tf.unstack(weights, num=Z_discrete_D, axis=1)
        biases = tf.layers.dense(code_embs, h_dim)  # (bs, D, h_dim^2)
        biases = tf.unstack(biases, num=Z_discrete_D, axis=1)
      return weights, biases

    weights, biases = hyper_net(Z, code_h_dim, h_dim)
    init = tf.get_variable("init_prior", [1, h_dim, 1])
    H = tf.tile(init, [tf.shape(Z)[0], 1, 1])
    for l in range(Z_discrete_D):#[::-1]: # DEBUG
      H_new = h_actv(tf.matmul(weights[l], H) + tf.expand_dims(biases[l], -1))
      if is_train:
        H_new = tf.nn.dropout(H_new, 1. - h_dropout)
      if struct_solution >= 2:
        H += H_new  # DEBUG
      else:
        H = H_new  # DEBUG
    # H = tf.layers.batch_normalization(H, training=is_train) # DEBUG

    H = tf.reshape(H, (-1, h_dim))
    if hparams.conv_deconv_baby:
      X_reconst = nets.dense.DECONV(H, x_actv, hparams, is_train)
    else:
      X_reconst = tf.layers.dense(H, X_shape, activation=x_actv)

  X_loss_batch = tf.reduce_sum((X_reconst - X)**2, 1)

  return X_reconst, X_loss_batch
