import tensorflow as tf
import gumbel as gb
import sampling
import nets
import util
gumbel_softmax = gb.gumbel_softmax
safer_log = util.safer_log

struct_solution = 4  # DEBUG. 1: nothing, 2: x2z_bn, z2x_residual, 3: x2z_bn, x2z_residual, z2x_residual, 4: x2z_bn, x2z_residual, z2x_bn, z2x_residual


def X2Z(X, pgor, hparams, is_train):
  """X->Z (no auto-regressive).

  Args:
    X: (batch_size, X_shape)

  Returns:
    Z: samples from posterior, (batch_size, D, K) if pgor is None, otherwise
      (pg_num_samples, batch_size, D, K).
    Z_prob: logits for Z, same shape as Z.
    B: (batch_size, D, K) discrete code in tf.int32.
    B_prior: (batch_size, D, K) discrete code sampled from prior in tf.int32.
    Z_KL_batch: (batch_size, ) if pgor is None, otherwise
      (pg_num_samples * batch_size, )
  """
  h_dim = hparams.X2Z_hdim
  h_dropout = hparams.X2Z_dropout
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  Z_sampling = hparams.Z_sampling
  Z_soft_KL = hparams.Z_soft_KL
  h_actv = util.get_activation(hparams.X2Z_hactv)
  batch_size = tf.shape(X)[0]

  H = 0.
  with tf.variable_scope("G_Z", reuse=not is_train):
    # Build a ladder, output logits for each level.
    logits = []
    for l in range(Z_discrete_D):
      H_in = X if l == 0 else H
      if struct_solution >= 2:
        H_in = tf.layers.batch_normalization(H_in, training=is_train)  # DEBUG
      H_new = tf.layers.dense(H_in, h_dim)
      H_new = h_actv(H_new)
      # H_new = tf.layers.batch_normalization(H_new, training=is_train)  # DEBUG
      if is_train:
        H_new = tf.nn.dropout(H_new, 1. - h_dropout)
      if struct_solution >= 3:
        H += H_new  # DEBUG
      else:
        H = H_new  # DEBUG
      H_in = H
      if struct_solution >= 2:
        H_in = tf.layers.batch_normalization(H_in, training=is_train)  # DEBUG
      logits_l = tf.layers.dense(H_in, Z_discrete_K)  # (batch_size, K)
      logits_l = tf.expand_dims(logits_l, -2)  # (batch_size, 1, K)
      logits.append(logits_l)
      # h_dim = h_dim // 2 # DEBUG
    logits = tf.concat(logits, -2) # (batch_size, D, K)

    # Sample Z/code.
    if pgor is None:
      Z, Z_soft, _ = gumbel_softmax(  # MAX instead of sampling during inference.
          logits, temperature=1., random=Z_sampling and is_train,
          straight_through=True, is_training=is_train)
      Z_prob = Z_soft
    else:
      Z, Z_prob = pgor.forward(logits, action_prob=True, is_training=is_train)

  # Gather discrete code.
  B = tf.cast(Z if pgor is None else Z[0], tf.int32)
  B_prior = sampling.discrete_prior_sampling(
      hparams.plot_batch_size, Z_discrete_D, Z_discrete_K,
      solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)

  # KL / Prior loss.
  Z4KL = Z_prob if Z_soft_KL else Z  # Note: it is constant for hard codes.
  Z_KL_batch = tf.reduce_sum(
      Z4KL * safer_log(Z4KL * Z_discrete_K), [-2, -1])
  Z_KL_batch = tf.reshape(Z_KL_batch, [-1])

  return Z, Z_prob, B, B_prior, Z_KL_batch


def Z2X(Z, X, hparams, is_train):
  """Z->X (no auto-regressive).

  Args:
    Z: latent posterior samples. (batch_size, D, K) if pgor is None, otherwise 
      (pg_num_samples, batch_size, D, K). It is also Okay to flatten D*K.
    X: targe/original X. (batch_size, X_shape)  if pgor is None, otherwise
      (pg_num_samples * batch_size, X_shape)

  Returns:
    X_reconst: (batch_size, X_shape) if pgor is None, otherwise
       (pg_num_samples * batch_size, X_shape)
    X_loss_batch: (batch_size, ) if pgor is None, otherwise
      (pg_num_samples * batch_size, )
  """
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  h_dim = hparams.Z2X_hdim
  h_dropout = hparams.Z2X_dropout
  h_actv = util.get_activation(hparams.Z2X_hactv)
  x_actv = util.get_activation(hparams.Z2X_xactv)
  X_shape = X.shape.as_list()[-1]
  # h_dim = h_dim // pow(2, Z_discrete_D)  # DEBUG

  with tf.variable_scope("G_X", reuse=not is_train):
    if len(Z.shape.as_list()) != 3:  # Skip if already shaped.
      Z = tf.reshape(Z, [-1, Z_discrete_D, Z_discrete_K])
    if hparams.Z_nested_dropout:
      Z = sampling.nested_dropout(Z, hparams, is_train)
    Z = tf.unstack(Z, num=Z_discrete_D, axis=-2)  # D of (pg_num_samples * batch_size, K)
    H = 0.
    for l in range(len(Z))[::-1]:
      if l == len(Z) - 1:
        H_in = Z[-1]
      else:
        H_in = tf.concat([H, Z[l]], -1)
      if struct_solution >= 4:
        H_in = tf.layers.batch_normalization(H_in, training=is_train) # DEBUG
      H_new = tf.layers.dense(H_in, h_dim)
      H_new = h_actv(H_new)
      if is_train:
        H_new = tf.nn.dropout(H_new, 1. - h_dropout)
      if struct_solution >= 2:
        H += H_new  # DEBUG
      else:
        H = H_new  # DEBUG
      # h_dim = h_dim * 2 # DEBUG
    # H = tf.layers.batch_normalization(H, training=is_train) # DEBUG

    if struct_solution >= 4:
      H = tf.layers.batch_normalization(H, training=is_train) # DEBUG
    if hparams.conv_deconv_baby:
      X_reconst = nets.dense.DECONV(H, x_actv, hparams, is_train)
    else:
      X_reconst = tf.layers.dense(H, X_shape, activation=x_actv)

  X_loss_batch = tf.reduce_sum((X_reconst - X)**2, 1)

  return X_reconst, X_loss_batch
