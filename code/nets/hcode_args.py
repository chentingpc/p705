import tensorflow as tf
import gumbel as gb
import sampling
import nets
import util
from hcode_vlae import Z2X as hcode_vlae_Z2X
gumbel_softmax = gb.gumbel_softmax
safer_log = util.safer_log

HP_bn = True
HP_residual = True

def X2Z(X, pgor, hparams, is_train):
  """X->Z (with auto-regressive).

  Args:
    X: (batch_size, X_shape)

  Returns:
    Z: samples from posterior, (batch_size, D, K) if pgor is None, otherwise
      (pg_num_samples, batch_size, D, K).
    Z_prob: logits for Z, same shape as Z.
    B: (batch_size, D, K) discrete code in tf.int32.
    B_prior: (batch_size, D, K) discrete code sampled from prior in tf.int32.
    Z_KL_batch: (batch_size, ) if pgor is None, otherwise
      (pg_num_samples * batch_size, )
  """
  h_dim = hparams.X2Z_hdim
  h_dropout = hparams.X2Z_dropout
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  Z_sampling = hparams.Z_sampling
  Z_soft_KL = hparams.Z_soft_KL
  h_actv = util.get_activation(hparams.X2Z_hactv)
  batch_size = tf.shape(X)[0]

  with tf.variable_scope("G_Z", reuse=not is_train):
    Zs, Z_probs = [], []
    H = 0.
    for l in range(Z_discrete_D):
      # Compute logits.
      if l == 0:
        H_new = tf.layers.dense(X, h_dim, activation=h_actv)
      else:
        H_new = tf.layers.dense(
            tf.concat([H, Z_in], -1), h_dim, activation=h_actv)
      if is_train:
          H_new = tf.nn.dropout(H_new, 1. - h_dropout)
      # H += H_new  # DEBUG
      H = H_new
      logits_l = tf.layers.dense(H, Z_discrete_K)  # (batch_size, K)

      # Sample Z/code.
      if pgor is None:
        Z, Z_soft, _ = gumbel_softmax(  # MAX instead of sampling during inference.
            logits_l, temperature=1., random=Z_sampling and is_train,
            straight_through=True, is_training=is_train)
        Z_prob = Z_soft
        Z_in = Z
      else:
        raise NotImplemented("only final forward used for backward, need fix.")  # DEBUG
        Z, Z_prob = pgor.forward(logits_l, action_prob=True, is_training=is_train)
        Z_in = Z[0]
      Zs.append(tf.expand_dims(Z, -2))
      Z_probs.append(tf.expand_dims(Z_prob, -2))
    Z = tf.concat(Zs, -2)
    Z_prob = tf.concat(Z_probs, -2)

  # Gather discrete code.
  B = tf.cast(Z if pgor is None else Z[0], tf.int32)
  B_prior = sampling.discrete_prior_sampling(
      hparams.plot_batch_size, Z_discrete_D, Z_discrete_K,
      solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)

  # KL / Prior loss.
  Z4KL = Z_prob if Z_soft_KL else Z  # Note: it is constant for hard codes.
  Z_KL_batch = tf.reduce_sum(
      Z4KL * safer_log(Z4KL * Z_discrete_K), [-2, -1])
  Z_KL_batch = tf.reshape(Z_KL_batch, [-1])

  return Z, Z_prob, B, B_prior, Z_KL_batch


def Z2X(Z, X, hparams, is_train):
  return hcode_vlae_Z2X(Z, X, hparams, is_train)


def X2Z2X(X, hparams, is_train):
  """X->Z-> (with auto-regressive on Z during decoding).

  Args:
    X: (batch_size, X_shape)

  Returns:
    Z: samples from posterior, (batch_size, D, K).
    Z_prob: logits for Z, same shape as Z.
    B: (batch_size, D, K) discrete code in tf.int32.
    B_prior: (batch_size, D, K) discrete code sampled from prior in tf.int32.
    Z_KL_batch: (batch_size, ).
    X_reconst: (batch_size, X_shape).
    X_loss_batch: (batch_size, ).
  """
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  Z_sampling = hparams.Z_sampling
  Z_soft_KL = hparams.Z_soft_KL
  enc_h_dim = hparams.X2Z_hdim
  enc_h_dropout = hparams.X2Z_dropout
  enc_h_actv = util.get_activation(hparams.X2Z_hactv)
  dec_h_dim = hparams.Z2X_hdim
  dec_h_dropout = hparams.Z2X_dropout
  dec_h_actv = util.get_activation(hparams.Z2X_hactv)
  dec_x_actv = util.get_activation(hparams.Z2X_xactv)
  batch_size = tf.shape(X)[0]
  X_shape = X.shape.as_list()[-1]

  # Encoder.
  with tf.variable_scope("G_Z", reuse=not is_train):
    Hs_enc = []
    H = 0.
    for l in range(Z_discrete_D):
      H_in = X if l == 0 else H
      if HP_bn:
        H_in = tf.layers.batch_normalization(H_in, training=is_train)  # DEBUG
      H_new = tf.layers.dense(H_in, enc_h_dim, activation=enc_h_actv)
      # H_new = tf.layers.batch_normalization(H_new, training=is_train)  # DEBUG
      if is_train:
        H_new = tf.nn.dropout(H_new, 1. - enc_h_dropout)
      if HP_residual:
        H += H_new  # DEBUG
      else:
        H = H_new
      Hs_enc.append(H)

  # Decoder.
  with tf.variable_scope("G_X", reuse=not is_train):
    Zs, Z_probs = [], []
    H = 0.
    for l in range(Z_discrete_D)[::-1]:
      # Sample Z/code.
      with tf.variable_scope("Z_sampling_{}".format(l), reuse=not is_train):
        H_in = Hs_enc[l] if l == Z_discrete_D - 1 else (
            tf.concat([Hs_enc[l], H], -1))
        if HP_bn:
          H_in = tf.layers.batch_normalization(H_in, training=is_train, name="BN_in_Z_%d"%l)  # DEBUG
        logits_l = tf.layers.dense(H_in, Z_discrete_K)
        Z, Z_soft, _ = gumbel_softmax(  # MAX instead of sampling during inference.
            logits_l, temperature=1., random=Z_sampling and is_train,
            straight_through=True, is_training=is_train)
        Zs.append(tf.expand_dims(Z, -2))
        Z_probs.append(tf.expand_dims(Z_soft, -2))
      # Compute new decoding h.
      if hparams.Z_nested_dropout:
        Z = sampling.nested_dropout_onedim(Z, l, hparams, is_train)
        # Z = sampling.progressive_dropout_onedim(Z, l, hparams, is_train)
      H_in = Z if l == Z_discrete_D - 1 else tf.concat([H, Z], -1)
      if HP_bn:
          H_in = tf.layers.batch_normalization(H_in, training=is_train, name="BN_in_H_%d"%l)  # DEBUG
      H_new = tf.layers.dense(H_in, dec_h_dim)
      H_new = dec_h_actv(H_new)
      if is_train:
        H_new = tf.nn.dropout(H_new, 1. - dec_h_dropout)
      if HP_residual:
        H += H_new  # DEBUG
      else:
        H = H_new
    # Reconst.
    if HP_bn:
      H = tf.layers.batch_normalization(H, training=is_train, name="BN_in_X_%d"%l) # DEBUG
    if hparams.conv_deconv_baby:
      X_reconst = nets.dense.DECONV(H, dec_x_actv, hparams, is_train)
    else:
      X_reconst = tf.layers.dense(H, X_shape, activation=dec_x_actv)
    # Combine results.
    Z = tf.concat(Zs[::-1], -2)
    Z_prob = tf.concat(Z_probs[::-1], -2)

  # X Loss.
  X_loss_batch = tf.reduce_sum((X_reconst - X)**2, 1)

  # Gather discrete code.
  B = tf.cast(Z, tf.int32)
  B_prior = sampling.discrete_prior_sampling(
      hparams.plot_batch_size, Z_discrete_D, Z_discrete_K,
      solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)

  # KL / Prior loss.
  Z4KL = Z_prob if Z_soft_KL else Z  # Note: it is constant for hard codes.
  Z_KL_batch = tf.reduce_sum(
      Z4KL * safer_log(Z4KL * Z_discrete_K), [-2, -1])
  Z_KL_batch = tf.reshape(Z_KL_batch, [-1])

  return Z, Z_prob, B, B_prior, Z_KL_batch, X_reconst, X_loss_batch


def Z2X_of_X2Z2X(Z, X, hparams, is_train=False):
  """Used for sampling X given Z when using X2Z2X."""
  if is_train:
    raise ValueError("This should only be called during inference.")
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  dec_h_dim = hparams.Z2X_hdim
  dec_h_dropout = hparams.Z2X_dropout
  dec_h_actv = util.get_activation(hparams.Z2X_hactv)
  dec_x_actv = util.get_activation(hparams.Z2X_xactv)
  X_shape = X.shape.as_list()[-1]

  with tf.variable_scope("G_X", reuse=not is_train):
    if len(Z.shape.as_list()) != 3:  # Skip if already shaped.
      Z = tf.reshape(Z, [-1, Z_discrete_D, Z_discrete_K])
    Z = tf.unstack(Z, num=Z_discrete_D, axis=-2)  # D of (pg_num_samples * batch_size, K)
    H = 0.
    for l in range(Z_discrete_D)[::-1]:
      # Compute new decoding h.
      H_in = Z[l] if l == Z_discrete_D - 1 else tf.concat([H, Z[l]], -1)
      if HP_bn:
        H_in = tf.layers.batch_normalization(H_in, training=is_train, name="BN_in_H_%d"%l) # DEBUG
      H_new = tf.layers.dense(H_in, dec_h_dim)
      H_new = dec_h_actv(H_new)
      if HP_residual:
        H += H_new  # DEBUG
      else:
        H = H_new

    if HP_bn:
      H = tf.layers.batch_normalization(H, training=is_train, name="BN_in_X_%d"%l) # DEBUG
    if hparams.conv_deconv_baby:
      X_reconst = nets.dense.DECONV(H, dec_x_actv, hparams, is_train)
    else:
      X_reconst = tf.layers.dense(H, X_shape, activation=dec_x_actv)

  return X_reconst, None