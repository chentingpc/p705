import tensorflow as tf
import gumbel as gb
import vq
import util
gumbel_softmax = gb.gumbel_softmax


def eps2Z(eps, z_dim, hparams, is_train=True):
  """Turns simple eps distribution to Z distribution using generator net.
  """
  h_dims = [hparams.eps2Z_hdim] * hparams.eps2Z_layers
  dropouts = [hparams.eps2Z_dropout] * hparams.eps2Z_layers
  h_actv = util.get_activation(hparams.eps2Z_hactv)

  H = eps
  for h_dim, dropout in zip(h_dims, dropouts):
    H = tf.layers.dense(H, h_dim, activation=h_actv)
    if is_train:
      H = tf.nn.dropout(H, 1. - dropout)
  Z = tf.layers.dense(H, z_dim)
  return Z


def X2Z_vq(X, K, D, d, hparams, vqi=None, is_train=True):
  """Returns (bacth_size, d)"""
  h_dims = [hparams.X2Zvq_hdim] * hparams.X2Zvq_layers
  dropouts = [hparams.X2Zvq_dropout] * hparams.X2Zvq_layers
  h_actv = util.get_activation(hparams.X2Zvq_hactv)
  kernel_initializer = None
  bias_initializer = tf.zeros_initializer

  if hparams.conv_deconv_baby:
    Z = CONV(X, D * d, hparams, is_train=is_train)
  else:
    H = X
    for h_dim, dropout in zip(h_dims, dropouts):
      H = tf.layers.dense(
          H, h_dim, activation=h_actv,
          kernel_initializer=kernel_initializer,
          bias_initializer=bias_initializer)
      if is_train:
        H = tf.nn.dropout(H, 1. - dropout)
    Z = tf.layers.dense(
        H, D * d,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer)
  Z = tf.reshape(Z, (-1, D, d))
  
  if vqi is None:
    vqi = vq.VectorQuantization(K, D, d)
  codes, Z = vqi.forward(Z, is_train=is_train)
  return codes, Z


def X2Z(X,
        z_dim,
        hparams,
        sampling=True,
        batch_norm=True,
        discrete_K=0,
        pgor=None,
        is_train=True):
  """Returns (bacth_size, d) if continuous, (bacth_size, D, K) if discrete_K."""
  h_dims = [hparams.X2Z_hdim] * hparams.X2Z_layers
  dropouts = [hparams.X2Z_dropout] * hparams.X2Z_layers
  K = discrete_K
  if discrete_K > 0:
    z_dim *= K
  h_actv = util.get_activation(hparams.X2Z_hactv)
  kernel_initializer = None
  bias_initializer = tf.zeros_initializer

  def _transform(H, head_scope, head_reuse=tf.AUTO_REUSE):
    """The scope is not applicable for the last layer."""
    with tf.variable_scope(head_scope, reuse=head_reuse):
      for h_dim, dropout in zip(h_dims, dropouts):
        H = tf.layers.dense(
            H, h_dim, activation=h_actv,
            kernel_initializer=kernel_initializer,
            bias_initializer=bias_initializer)
        if is_train:
          H = tf.nn.dropout(H, 1. - dropout)
    H = tf.layers.dense(
        H, z_dim,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer)
    return H

  if not sampling:
    if hparams.conv_deconv_baby:
      H = CONV(X, z_dim, hparams, is_train=is_train)
    else:
      H = _transform(X, "Z_head")
    if discrete_K > 0:
      Z = tf.reshape(H, (-1, z_dim / K, K))
      if pgor is None:
        # tpack = ("inv_time", 100, 10, 1., 1e-6)  # TODO
        # tpack = ("exp", 100, 0.95, 1., 1e-6)
        tpack = 1.
        Z, Z_soft, _ = gumbel_softmax(
            Z, temperature=tpack, random=False,
            straight_through=True, is_training=is_train)
        #Z = gb.straight_through(
        #    Z, thresholding=False, softmax=True, hard=True,
        #    temperature=tpack, is_training=is_train)
        Z_prob = Z_soft
      else:
        Z, Z_prob = pgor.forward(Z, action_prob=True, is_training=is_train)
    else:
      if batch_norm:  # Note: normalize the last dim.
        Z = tf.layers.batch_normalization(Z, training=is_train)
    Z_mean, Z_std = None, None
  else:
    if discrete_K > 0:
      if hparams.conv_deconv_baby:
        H = CONV(X, z_dim, hparams, is_train=is_train)
      else:
        H = _transform(X, "Z_head")
      Z = tf.reshape(H, (-1, z_dim / K, K))

      if pgor is None:
        Z, Z_soft, _ = gumbel_softmax(  # MAX instead of sampling during inference.
            Z, temperature=1., random=is_train,
            straight_through=True, is_training=is_train)
        Z_prob = Z_soft
      else:
        Z, Z_prob = pgor.forward(Z, action_prob=True, is_training=is_train)
      Z_mean, Z_std = None, None
    else:
      # The head can be shared or not.
      if hparams.conv_deconv_baby:
        Zs = CONV(X, 2 * z_dim, hparams, is_train=is_train)
        Z_mean, Z_std = Zs[:, :z_dim], Zs[:, z_dim:]
      else:
        Z_mean, Z_std = _transform(X, "Z_mean_head"), _transform(X, "Z_std_head")
        # Z_mean, Z_std = _transform(X, "Z_head"), _transform(X, "Z_head")

      if batch_norm:  # Note: normalize the last dim.
        Z_mean = tf.layers.batch_normalization(Z_mean, training=is_train)
      if is_train or hparams.test_summary:
        tf.summary.histogram("z_mean_hist", Z_mean)
        tf.summary.histogram("z_std_hist", Z_std)
      Z = Z_mean + tf.random_normal(tf.shape(Z_mean)) * Z_std

  if is_train or hparams.test_summary:
    tf.summary.histogram("Z", Z)
  if discrete_K > 0:
    return Z, Z_prob
  else:
    return Z, Z_mean, Z_std


def Z2X(Z,
        x_dim,
        hparams,
        batch_norm=False,
        is_train=True):
  h_dims = [hparams.Z2X_hdim] * hparams.Z2X_layers
  dropouts = [hparams.Z2X_dropout] * hparams.Z2X_layers
  h_actv = util.get_activation(hparams.Z2X_hactv)
  x_actv = util.get_activation(hparams.Z2X_xactv)

  if hparams.conv_deconv_baby:
    X = DECONV(Z, x_actv, hparams, is_train)
  else:
    H = Z
    for h_dim, dropout in zip(h_dims, dropouts):
      H = tf.layers.dense(H, h_dim, activation=h_actv)
      if batch_norm:
        H = tf.layers.batch_normalization(H, training=is_train)
      if is_train:
        H = tf.nn.dropout(H, 1. - dropout)
    # H = tf.layers.batch_normalization(H, training=is_train)  # DEBUG: systematically add it.
    X = tf.layers.dense(H, x_dim, activation=x_actv)
  return X


def D(X,
      output_activation,
      hparams,
      is_train=True):
  h_dims = [hparams.D_hdim] * hparams.D_layers
  dropouts = [hparams.D_dropout] * hparams.D_layers
  h_actv = util.get_activation(hparams.D_hactv)

  if hparams.conv_deconv_baby:
    if output_activation is None:
      output_activation = lambda x: x
    Y = output_activation(CONV(X, 1, hparams, is_train))
  else:
    H = X
    for h_dim, dropout in zip(h_dims, dropouts):
      H = tf.layers.dense(H, h_dim)
      # H = tf.layers.batch_normalization(H, training=is_train)  # DEBUG
      H = h_actv(H)
      if is_train:
        H = tf.nn.dropout(H, 1. - dropout)
    Y = tf.layers.dense(H, 1, activation=output_activation)
  return Y


def Z2Y(Z, Y, hparams, Y_pred_onehot=True, is_train=True):
  """
  Args:
    Z: latent features of (batch_size, output_dim)
    Y: labels of (batch_size, output_dim) in one-hot format.

  Returns:
    Y_pred: predictions of (batch_size, 1) or (batch_size, output_dim) if
      Y_pred_onehot is set True.
    loss: a scalar cross entropy loss.
  """
  h_dims = [hparams.Z2Y_hdim] * hparams.Z2Y_layers
  dropouts = [hparams.Z2Y_dropout] * hparams.Z2Y_layers
  h_actv = util.get_activation(hparams.Z2Y_hactv)
  output_dim = Y.shape.as_list()[-1]

  H = Z
  for h_dim, dropout in zip(h_dims, dropouts):
    H = tf.layers.dense(H, h_dim, activation=h_actv)
    if is_train:
      H = tf.nn.dropout(H, 1. - dropout)
  Y_logits = tf.layers.dense(H, output_dim)

  Y_pred = tf.argmax(Y_logits, -1)
  if Y_pred_onehot:
    Y_pred = tf.one_hot(Y_pred, output_dim)

  loss = tf.reduce_mean(
      tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=Y_logits))

  return Y_pred, loss


def CONV(X, output_dim, hparams, is_train):
  """Take X, conv it and output H. 

  MNIST:
  (28, 28, 1) -> (14, 14, 64) -> (7, 7, 128) -> 1024 -> output_dim

  CIFAR:
  (32, 32, 3) -> (16, 16, 64) -> (8, 8, 128) -> (4, 4, 256) -> 1024 -> output_dim

  Args:
    X: (batch_size, d).
  """
  # DEBUG. consider shape and hparams for more data sets.
  from ops_ext import *
  batch_size = tf.shape(X)[0]
  dataset = hparams.task_name
  assert dataset in ["mnist", "cifar10", "cifar100"]

  if dataset == "mnist":
    # MNIST
    X = tf.reshape(X, [batch_size, 28, 28, 1])
    net = lrelu(conv2d(X, 64, 4, 4, 2, 2, name='en_conv1'))
    net = lrelu(bn(conv2d(net, 128, 4, 4, 2, 2, name='en_conv2'),
                   is_training=is_train, scope='en_bn2'))
    net = tf.reshape(net, [batch_size, 128 * 7 * 7])
    net = lrelu(bn(linear(net, 1024, scope='en_fc3'),
                   is_training=is_train, scope='en_bn3'))
    out = linear(net, output_dim, scope='en_fc4')
  else:
    # CIFAR:
    X = tf.reshape(X, [batch_size, 32, 32, 3])
    net = lrelu(conv2d(X, 64, 4, 4, 2, 2, name='en_conv1'))
    net = lrelu(bn(conv2d(net, 128, 4, 4, 2, 2, name='en_conv2'),
                   is_training=is_train, scope='en_bn2'))
    net = lrelu(bn(conv2d(net, 256, 4, 4, 2, 2, name='en_conv3'),
                   is_training=is_train, scope='en_bn3'))
    net = tf.reshape(net, [batch_size, 256 * 4 * 4])
    net = lrelu(bn(linear(net, 1024, scope='en_fc4'),
                   is_training=is_train, scope='en_bn4'))
    out = linear(net, output_dim, scope='en_fc5')

  return out


def DECONV(Z, output_actv, hparams, is_train):
  """Take Z, deconv it and output X. 
  
  MNIST:
  input_dim -> 1024 -> (7, 7, 128) -> (14, 14, 64) -> (28, 28, 1)

  CIFAR:
  input_dim -> 1024 -> (4, 4, 256) -> (8, 8, 128) -> (16, 16, 64) -> (32, 32, 3)

  Args:
    X: (batch_size, d).
  """
  # DEBUG. consider shape and hparams for more data sets.
  from ops_ext import *
  batch_size = tf.shape(Z)[0]
  dataset = hparams.task_name
  assert dataset in ["mnist", "cifar10", "cifar100"]

  if dataset == "mnist":
    # MNIST
    net = tf.nn.relu(bn(linear(Z, 1024, scope='de_fc1'),
                        is_training=is_train, scope='de_bn1'))
    net = tf.nn.relu(bn(linear(net, 128 * 7 * 7, scope='de_fc2'),
                        is_training=is_train, scope='de_bn2'))
    net = tf.reshape(net, [batch_size, 7, 7, 128])
    net = tf.nn.relu(bn(deconv2d(net,
                                 [batch_size, 14, 14, 64], 4, 4, 2, 2,
                                 name='de_dc3'),
                        is_training=is_train, scope='de_bn3'))
    out = output_actv(deconv2d(net,
                               [batch_size, 28, 28, 1], 4, 4, 2, 2,
                               name='de_dc4'))
  else:
    # CIFAR
    net = tf.nn.relu(bn(linear(Z, 1024, scope='de_fc1'),
                        is_training=is_train, scope='de_bn1'))
    net = tf.nn.relu(bn(linear(net, 256 * 4 * 4, scope='de_fc2'),
                        is_training=is_train, scope='de_bn2'))
    net = tf.reshape(net, [batch_size, 4, 4, 256])
    net = tf.nn.relu(bn(deconv2d(net,
                                 [batch_size, 8, 8, 128], 4, 4, 2, 2,
                                 name='de_dc3'),
                        is_training=is_train,scope='de_bn3'))
    net = tf.nn.relu(bn(deconv2d(net,
                                 [batch_size, 16, 16, 64], 4, 4, 2, 2,
                                 name='de_dc4'),
                        is_training=is_train,scope='de_bn4'))
    out = output_actv(deconv2d(net,
                               [batch_size, 32, 32, 3], 4, 4, 2, 2,
                               name='de_dc5'))
  out = tf.reshape(out, [batch_size, -1])

  return out