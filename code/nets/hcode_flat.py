import tensorflow as tf
import sampling
import nets
import util
log = util.safer_log


def X2Z(X, pgor, hparams, is_train):
  """X->Z.

  Args:
    X: (batch_size, X_shape)

  Returns:
    Z: samples from posterior, (batch_size, D, K) if pgor is None, otherwise
      (pg_num_samples, batch_size, D, K).
    Z_prob: logits for Z, same shape as Z.
    B: (batch_size, D, K) discrete code in tf.int32.
    B_prior: (batch_size, D, K) discrete code sampled from prior in tf.int32.
    Z_KL_batch: (batch_size, ) if pgor is None, otherwise
      (pg_num_samples * batch_size, )
  """
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  Z_sampling = hparams.Z_sampling
  Z_soft_KL = hparams.Z_soft_KL
  batch_size = tf.shape(X)[0]

  with tf.variable_scope("G_Z", reuse=not is_train):
    Z, Z_prob = nets.dense.X2Z(
        X,                        # (batch_size, D, K) if pgor is None, else
        Z_discrete_D,             # (pg_num_samples, batch_size, D, K).
        hparams,                  # The pg_first_n4f of pg_num_samples are
        sampling=Z_sampling,      # sampled from policy, will be used for
        discrete_K=Z_discrete_K,  # optimizing rewarding/decoder function.
        pgor=pgor,                # The Z_prob is of the same shape.
        is_train=is_train)
    B = tf.cast(Z if pgor is None else Z[0], tf.int32)
  B_prior = sampling.discrete_prior_sampling(
      hparams.plot_batch_size, Z_discrete_D, Z_discrete_K,
      solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)

  Z4KL = Z_prob if Z_soft_KL else Z  # Note: it is constant for hard codes.
  Z_KL_batch = tf.reduce_sum(
      Z4KL * log(Z4KL * Z_discrete_K), [-2, -1])
  Z_KL_batch = tf.reshape(Z_KL_batch, [-1])

  return Z, Z_prob, B, B_prior, Z_KL_batch


def Z2X(Z, X, hparams, is_train):
  """Returns reconstructed data/images.
  
  Args:
    Z: latent posterior samples. (batch_size, D, K) if pgor is None, otherwise 
      (pg_num_samples, batch_size, D, K).
    X: targe/original X. (batch_size, X_shape)  if pgor is None, otherwise
      (pg_num_samples * batch_size, X_shape)

  Returns:
    X_reconst: (batch_size, X_shape) if pgor is None, otherwise
       (pg_num_samples * batch_size, X_shape)
    X_loss_batch: (batch_size, ) if pgor is None, otherwise
      (pg_num_samples * batch_size, )
  """
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  X_shape = X.shape.as_list()[-1]

  with tf.variable_scope("G_X", reuse=not is_train):
    if hparams.Z_nested_dropout:
      Z = sampling.nested_dropout(Z, hparams, is_train)
      # Z = sampling.progressive_dropout(Z, hparams, is_train)
    Z = tf.reshape(Z, [-1, Z_discrete_D * Z_discrete_K])
    X_reconst = nets.dense.Z2X(Z, X_shape, hparams, is_train=is_train)

  X_loss_batch = tf.reduce_sum((X_reconst - X)**2, 1)
  
  return X_reconst, X_loss_batch

# Following is for auto-encoding 

def ae_X2Z(X, pgor_useless, hparams, is_train):
  """X->Z.

  Args:
    X: (batch_size, X_shape)

  Returns:
    Z: samples from posterior of shape (batch_size, d),
      d = D * K if use_discrete_code, otherwise d = noises_dim.
    Z_prob: logits for Z if use_discrete_code, otherwise None.
    Z_KL_batch: (batch_size, ).
  """
  Z_sampling = hparams.Z_sampling
  Z_soft_KL = hparams.Z_soft_KL
  noises_dim = hparams.noises_dim
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  Z_batch_norm = hparams.Z_batch_norm
  use_discrete_code = Z_discrete_D > 0
  batch_size = tf.shape(X)[0]

  with tf.variable_scope("G_Z", reuse=not is_train):
    X_noise = tf.random_normal(tf.shape(X))
    X = tf.concat([X, X_noise], 1) if Z_sampling == -1 else X
    if use_discrete_code:
      Z, Z_prob = nets.dense.X2Z(
          X,
          Z_discrete_D,
          hparams,
          sampling=Z_sampling > 0,
          batch_norm=Z_batch_norm,
          discrete_K=Z_discrete_K,
          is_train=is_train)
    else:
      Z, Z_mean, Z_std = nets.dense.X2Z(
          X,
          noises_dim,
          hparams,
          sampling=Z_sampling > 0,
          batch_norm=Z_batch_norm,
          is_train=is_train)
      Z_prob = None

  if use_discrete_code:
    Z4KL = Z_prob if Z_soft_KL else Z  # Note: it is constant for hard codes.
    Z_KL_batch = tf.reduce_sum(Z4KL * log(Z4KL * Z_discrete_K), [-2, -1])
    Z_KL_batch = tf.reshape(Z_KL_batch, [-1])
  else:
    if Z_mean is None:
      Z_KL_batch = tf.zeros((batch_size, ))
    else:
      Z_KL_batch = -.5 * tf.reduce_sum(
          1 + log(Z_std**2) - Z_mean**2 - Z_std**2, 1)

  return Z, Z_prob, None, None, Z_KL_batch  # None is for API consistent


def ae_Z2X(Z, X, hparams, is_train):
  """Returns reconstructed data/images.
  
  Args:
    Z: latent posterior samples. (batch_size, d),
      d = D * K if use_discrete_code, otherwise d = noises_dim.
    X: targe/original X. (batch_size, X_shape)

  Returns:
    X_reconst: (batch_size, X_shape).
    X_loss_batch: (batch_size, ).
  """
  batch_size = tf.shape(Z)[0]
  X_shape = X.shape.as_list()[-1]
  noises_dim = hparams.noises_dim
  Z_discrete_K = hparams.Z_discrete_K
  Z_discrete_D = hparams.Z_discrete_D
  use_discrete_code = Z_discrete_D > 0

  with tf.variable_scope("G_X", reuse=not is_train):
    if use_discrete_code and len(Z.shape.as_list()) != 2:  # Skip if already shaped.
      Z = tf.reshape(Z, [batch_size, Z_discrete_D * Z_discrete_K])
    X_reconst = nets.dense.Z2X(Z, X_shape, hparams, is_train=is_train)

  X_loss_batch = tf.reduce_sum((X_reconst - X)**2, 1)
  
  return X_reconst, X_loss_batch
