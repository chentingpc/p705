import numpy as np
import tensorflow as tf
import gumbel as gb


class TestGumbel(tf.test.TestCase):
  def testGumbelSoftmax(self):
    tf.set_random_seed(100)
    logits = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]])
    logits = tf.constant(logits, dtype=tf.float32)
    b, z, zb = gb.gumbel_softmax(
        logits, temperature=1., random=True, straight_through=False)
    if zb is not None:
      zb_grad = tf.gradients(tf.reduce_sum(zb**2), logits)[0]
    with self.test_session() as sess:
      fetches = {"b": b, "z": z}
      if zb is not None:
        fetches["zb"] = zb
        fetches["zb_grad"] = zb_grad
      results = sess.run(fetches)
      for key, val in results.iteritems():
        print key
        print np.round(val, 3)

  def testGumbelBinary(self):
    tf.set_random_seed(100)
    logits = np.array([0.5, 1, -0.5, -1])
    logits = tf.constant(logits, dtype=tf.float32)
    b, z, zb = gb.gumbel_binary(
        logits, temperature=1., random=True, straight_through=False)
    if zb is not None:
      zb_grad = tf.gradients(tf.reduce_sum(zb**2), logits)
    with self.test_session() as sess:
      fetches = {"b": b, "z": z}
      if zb is not None:
        fetches["zb"] = zb
        fetches["zb_grad"] = zb_grad
      results = sess.run(fetches)
      for key, val in results.iteritems():
        print key
        print np.round(val, 3)

  def testGumbelSoftmaxVsBinary(self):
    tf.set_random_seed(100)
    N = 10000
    for prob in range(1, 10):
      prob /= 10.
      logits_binary = np.array([prob] * N)
      logits_binary = tf.constant(logits_binary, dtype=tf.float32)
      logits_softmax = np.array([[1. - prob, prob]] * N)
      logits_softmax = tf.constant(logits_softmax, dtype=tf.float32)
      def run(logits, gumbel_machine):
        b, z, zb = gumbel_machine(
            logits, temperature=1., random=True, logits_are_probas=True)
        with self.test_session() as sess:
          return sess.run({"b": b, "z": z, "zb": zb})
      results_binary = run(logits_binary, gb.gumbel_binary)
      results_softmax = run(logits_softmax, gb.gumbel_softmax)
      for key, vals_binary in results_binary.iteritems():
        vals_softmax = results_softmax[key]
        val_binary = np.mean(vals_binary)
        val_softmax = np.mean(vals_softmax, 0)[1]
        self.assertAlmostEqual(val_binary, val_softmax, delta=0.05)
        val_binary = np.std(vals_binary)
        val_softmax = np.std(vals_softmax, 0)[1]
        self.assertAlmostEqual(val_binary, val_softmax, delta=0.05)

if __name__ == "__main__":
  tf.test.main()
