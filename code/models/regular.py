from functools import partial
import numpy as np
import tensorflow as tf
import nets
import sampling
import util
from loss import get_gan_loss
# Warning: tf.nn.elu has a bug, seems to be related to second order gradient,
# and using it in Discriminator can make optimization very unstable.

def get_model(images,
              labels,
              loss_type,
              hparams,
              is_train=True):
  """Returns train_ops, losses, and sampled images.
  """
  noises_dim = hparams.noises_dim
  optimizer = hparams.optimizer
  learning_rate = hparams.learning_rate
  max_grad_norm = hparams.max_grad_norm
  Z_use_label = hparams.Z_use_label
  Z_discrete_K = hparams.Z_discrete_K  # K in KD code
  Z_discrete_D = hparams.Z_discrete_D  # D in KD code
  discrete_coding_scheme = hparams.discrete_coding_scheme
  use_discrete_code = Z_discrete_D > 0
  print("use_discrete_code: {}".format(use_discrete_code))

  # Be careful with changing the following setting.
  dis_op_drop_rate = 0.  # Chance of dis_train_op being dismissed.
  gen_op_drop_rate = 0.  # Chance of gen_train_op being dismissed.
  noise_transform_by_discriminator = False  # This seems to be poorly motivated.

  batch_size = tf.shape(images)[0]
  images_shape = images.shape.as_list()[1:]
  images_shape_flat = reduce(np.multiply, images_shape)
  images = tf.reshape(images, [-1, images_shape_flat])
  if use_discrete_code:
    epsilon = sampling.discrete_prior_sampling(
        batch_size, Z_discrete_D, Z_discrete_K, solo_dim=False, hparams=hparams)
    epsilon_out  = sampling.discrete_prior_sampling(
        hparams.plot_batch_size, Z_discrete_D, Z_discrete_K,
        solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)
  else:
    epsilon = sampling.continuous_prior_sampling(
        batch_size, noises_dim, solo_dim=False, hparams=hparams)
    epsilon_out = sampling.continuous_prior_sampling(
        hparams.plot_batch_size, noises_dim,
        solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)
  if noise_transform_by_discriminator:
    with tf.variable_scope("Discriminator_eps", reuse=not is_train):
      noises = nets.dense.eps2Z(
          epsilon, noises_dim, hparams, is_train=is_train)
    with tf.variable_scope("Discriminator_eps", reuse=True):
      noises_out = nets.dense.eps2Z(
          epsilon_out, noises_dim, hparams, is_train=is_train)
  else:
      noises = epsilon
      noises_out = epsilon_out
  with tf.variable_scope("Generator", reuse=not is_train):
    Z = tf.concat([noises, labels], -1) if Z_use_label else noises
    if use_discrete_code:
      if discrete_coding_scheme == "flat":
        Z2X_function = nets.hcode_flat.Z2X
      elif discrete_coding_scheme == "vlae":
        Z2X_function = nets.hcode_vlae.Z2X
      elif discrete_coding_scheme == "hyper":
        Z2X_function = nets.hcode_hyper.Z2X
      else:
        raise ValueError("Unknown discrete_coding_scheme {}".format(
            discrete_coding_scheme))
      images_fake, _ = Z2X_function(
          tf.cast(Z, tf.float32), images, hparams, is_train=is_train)
    else:
      images_fake = nets.dense.Z2X(
          Z, images_shape_flat, hparams, is_train=is_train)
  with tf.variable_scope("Generator", reuse=True):
    Z_out = tf.concat([noises_out, labels], -1) if Z_use_label else noises_out
    if use_discrete_code:
      images_out, _ = Z2X_function(
          tf.cast(Z_out, tf.float32), images, hparams, is_train=is_train)
    else:
      images_out = nets.dense.Z2X(
          Z_out, images_shape_flat, hparams, is_train=is_train)

  D_out_actv = tf.nn.sigmoid if loss_type == "logistic" else None
  D = tf.make_template("Discriminator", nets.dense.D, hparams=hparams,
                       output_activation=D_out_actv, is_train=is_train)

  score_real = D(images)
  score_fake = D(images_fake)
  dis_loss, gen_loss = get_gan_loss(
      loss_type, score_real, score_fake, D, images, images_fake)

  if is_train:
    dis_theta = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/Discriminator")
    gen_theta = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/Generator")
    optimizer = util.get_optimizer(optimizer)
    optimizer = optimizer(learning_rate=learning_rate)

    if dis_op_drop_rate == 0.:
      dis_loss_for_train = dis_loss
    else:
      dis_loss_for_train = dis_loss * tf.cast(
          tf.random_uniform([]) > dis_op_drop_rate, tf.float32)
    dis_train_op = tf.contrib.layers.optimize_loss(
        loss=dis_loss_for_train,
        global_step=tf.train.get_or_create_global_step(),
        learning_rate=learning_rate,
        variables=dis_theta,
        optimizer=optimizer,
        clip_gradients=max_grad_norm,
        name="Discriminator")
    if gen_op_drop_rate == 0.:
      gen_loss_for_train = gen_loss
    else:
      gen_loss_for_train = gen_loss * tf.cast(
          tf.random_uniform([]) > gen_op_drop_rate, tf.float32)
    gen_train_op = tf.contrib.layers.optimize_loss(
        loss=gen_loss_for_train,
        global_step=tf.train.get_or_create_global_step(),
        learning_rate=learning_rate,
        variables=gen_theta,
        optimizer=optimizer,
        clip_gradients=max_grad_norm,
        name="Generator")
    train_ops = {"D": dis_train_op,
                 "G": gen_train_op}
  else:
    train_ops = None

  if is_train or hparams.test_summary:
    tf.summary.scalar("dis_loss", dis_loss)
    tf.summary.scalar("gen_loss", gen_loss)
  losses = {"dis_loss": dis_loss,
            "gen_loss": gen_loss}
  Z_random = tf.argmax(epsilon_out, -1) if use_discrete_code else epsilon_out
  samples = {"Z_random": Z_random,
             "images_random": images_out}
  
  return train_ops, losses, samples
