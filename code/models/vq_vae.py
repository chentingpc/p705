import numpy as np
import tensorflow as tf
import nets
import sampling
import util
import vq
log = util.safer_log
from loss import get_gan_loss


def get_model(images,
              labels,
              loss_type,
              hparams,
              idxs=None,
              num_points=None,
              is_train=True):
  """Returns train_ops, losses, and sampled images.
  """
  # Default parameters.
  optimizer = hparams.optimizer
  learning_rate = hparams.learning_rate
  max_grad_norm = hparams.max_grad_norm
  discrete_coding_scheme = hparams.discrete_coding_scheme
  Z_discrete_K = hparams.Z_discrete_K  # K in KD code
  Z_discrete_D = hparams.Z_discrete_D  # D in KD code
  Z_d = hparams.Z_d  # d of code embedding
  reg_weight = hparams.vq_reg_weight
  shared_codebook = hparams.vq_shared_codebook
  code_emb_merger = hparams.vq_code_emb_merger
  use_X2Z_net = True
  if discrete_coding_scheme != "flat":
    raise NotImplemented("vq_vae currently only supports flat "
                         "discrete_coding_scheme.")

  #--- House keeping ---
  batch_size = tf.shape(images)[0]
  images_shape = images.shape.as_list()[1:]
  images_shape_flat = reduce(np.multiply, images_shape)
  images = tf.reshape(images, [-1, images_shape_flat])
  codes_random = sampling.discrete_prior_sampling(
      hparams.plot_batch_size, Z_discrete_D, Z_discrete_K,
      solo_dim=hparams.solo_dim_prior_sampling,
      one_hot=False, hparams=hparams)

  vqi = vq.VectorQuantization(
      Z_discrete_K, Z_discrete_D, Z_d, shared_codebook=shared_codebook)

  #--- X->Z ---
  with tf.variable_scope("G_Z"):
    if use_X2Z_net:
      X2Z_input = images
    else:
      X2Z_input = tf.one_hot(idxs, num_points)
    codes, noises_fake = nets.dense.X2Z_vq(  # codes, and code embeddings.
        X2Z_input,
        Z_discrete_K,
        Z_discrete_D,
        Z_d,
        hparams,
        vqi=vqi,
        is_train=is_train)
    codes = tf.cast(tf.reshape(tf.one_hot(codes, Z_discrete_K),
                               [-1, Z_discrete_D * Z_discrete_K]),
                    tf.int32)
    noises = vqi.code2embs(codes_random)
    if code_emb_merger == "average":
      noises_fake = tf.reduce_mean(noises_fake, 1)
      noises = tf.reduce_mean(noises, 1)  
    elif code_emb_merger == "concat":
      noises_fake = tf.reshape(noises_fake, [-1, Z_discrete_D * Z_d])
      noises = tf.reshape(noises, [-1, Z_discrete_D * Z_d])
    else:
      raise ValueError()

  #--- Z->X ---
  with tf.variable_scope("G_X"):
    images_fake = nets.dense.Z2X(
        noises_fake, images_shape_flat, hparams, is_train=is_train)
  D_out_actv = tf.nn.sigmoid if loss_type == "logistic" else None
  X_reconst_loss = tf.reduce_mean(tf.reduce_sum((images_fake - images)**2, 1))
  if is_train or hparams.test_summary:
    tf.summary.scalar("X_reconst_loss", X_reconst_loss)

  #--- losses ---
  dis_loss = tf.constant(0.)
  gen_loss = X_reconst_loss

  if is_train:
    gen_theta = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_X")
    gen_theta += tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_Z")
    optimizer = util.get_optimizer(optimizer)
    optimizer = optimizer(learning_rate=learning_rate)

    reg = sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
    if is_train or hparams.test_summary:
      tf.summary.scalar("reg", reg)
    gen_loss += reg * reg_weight
    
    if hparams.weight_supervised_loss != 0:
      with tf.variable_scope("G_Z/Z2Y"):
        preds, super_loss = nets.dense.Z2Y(
            noises_fake, labels, hparams, is_train=is_train)
      gen_loss += super_loss * hparams.weight_supervised_loss
    else:
      preds = None

    gen_train_op = tf.contrib.layers.optimize_loss(
        loss=gen_loss,
        global_step=tf.train.get_or_create_global_step(),
        learning_rate=learning_rate,
        variables=gen_theta,
        optimizer=optimizer,
        clip_gradients=max_grad_norm,
        name="Generator")

    train_ops = {"D": tf.constant(0., name="constant_noop"),
                 "G": gen_train_op}
  else:
    train_ops = None


  losses = {"dis_loss": dis_loss,
            "gen_loss": gen_loss}
  with tf.variable_scope("G_X", reuse=True):
    images_random = nets.dense.Z2X(
        noises, images_shape_flat, hparams, is_train=is_train)
  samples = {"Z_random": codes_random,  # (batch_size, D)
             "images_random": images_random,
             "images_reconst": images_fake,
             "labels": labels,
             "preds": preds,
             "codes": codes,
             "reconst_loss": X_reconst_loss}

  return train_ops, losses, samples
