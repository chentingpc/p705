import numpy as np
import tensorflow as tf
import nets
import sampling
import util
log = util.safer_log
from loss import get_gan_loss


def get_model(images,
              labels,
              loss_type,
              hparams,
              is_train=True):
  """Returns train_ops, losses, and sampled images.
  """
  optimizer = hparams.optimizer
  learning_rate = hparams.learning_rate
  max_grad_norm = hparams.max_grad_norm
  Z_use_label = hparams.Z_use_label
  noise_transform = hparams.noise_transform # "gen": Z=f(\eps), trained as part of generator.
  noises_dim = hparams.noises_dim
  Z_discrete_K = hparams.Z_discrete_K  # K in KD code
  Z_discrete_D = hparams.Z_discrete_D  # D in KD code
  discrete_coding_scheme = hparams.discrete_coding_scheme
  use_discrete_code = Z_discrete_D > 0
  print("use_discrete_code: {}".format(use_discrete_code))

  # Be careful with changing the following setting.
  dis_op_drop_rate = 0.  # Chance of dis_train_op being dismissed.
  gen_op_drop_rate = 0.  # Chance of gen_train_op being dismissed.

  # ====================== Configs for variants (BEGIN) ====================== #
  Z_from_X = hparams.Z_from_X  # Use with Z_sampling and Z_batch_norm.
  Z_sampling = hparams.Z_sampling  # 0: not sampling, 1: Z~N(.|X, .|X), -1: Z=G(X, mu), mu~N(0,1).
  Z_batch_norm = hparams.Z_batch_norm
  Z_use_gan = hparams.Z_use_gan
  Z_use_KL = hparams.Z_use_KL
  X_use_gan = hparams.X_use_gan
  X_use_reconst = hparams.X_use_reconst
  XZ_joint_gan = hparams.XZ_joint_gan  # Only feasible when both Z_use_gan & X_use_gan are True.

  # Following are prototypes: vae, ae_gan, gan_vae, ali, ae_ali
  prototype = hparams.auto_prototype

  # VAE: no GAN used.
  if prototype == "vae":
    Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
      True, 1., False, False, 1., False, True, False)
    #Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
    #  True, 1, False, False, 0, False, True, False)
    #Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
    #  True, 0, False, False, 0, False, True, False)
  #Z_sampling = False
  #Z_use_KL = .0  # DEBUG

  # GAN
  if prototype == "gan":
    Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
      False, 0, False, False, False, True, False, False)

  # AE-GAN: GAN is used for latent space of Auto-encoder.
  if prototype == "ae_gan":
    Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
      True, -1 if noise_transform == "gen" else 1, True, True, False, False, True, False)

  # GAN-VAE: GAN is used for the data space of Auto-encoder.
  if prototype == "gan_vae":
    Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
      True, 1, False, False, True, True, False, False)

  # ALI
  if prototype == "ali":
    Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
      False, 0, False, True, False, True, False, True)

  # AE-ALI: similar to ALI, but the Z~Q(Z|X) instead of Z~P(Z).
  if prototype == "ae_ali":
    Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
      True, -1, True, True, False, True, False, True)

  # Sanity check: compare x->z->\hat{x} VS x. prototype = sanity_$Z_sampling.
  if prototype.startswith("sanity"):
    Z_sampling = int(prototype.split('_')[1])
    Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan = (
      True, Z_sampling, True, False, False, True, False, False)

  # Set them in hparams so that function call with hparams can see new hparams.
  (hparams.Z_from_X, hparams.Z_sampling, hparams.Z_batch_norm, hparams.Z_use_gan,
  hparams.Z_use_KL, hparams.X_use_gan, hparams.X_use_reconst, hparams.XZ_joint_gan) = (
  Z_from_X, Z_sampling, Z_batch_norm, Z_use_gan, Z_use_KL, X_use_gan, X_use_reconst, XZ_joint_gan)
  # ======================= Configs for variants (END) ======================= #

  # Housing keeping.
  batch_size = tf.shape(images)[0]
  images_shape = images.shape.as_list()[1:]
  images_shape_flat = reduce(np.multiply, images_shape)
  images = tf.reshape(images, [-1, images_shape_flat])
  if use_discrete_code:
    epsilon = sampling.discrete_prior_sampling(
        batch_size, Z_discrete_D, Z_discrete_K, solo_dim=False, hparams=hparams)
    epsilon_out  = sampling.discrete_prior_sampling(
        hparams.plot_batch_size, Z_discrete_D, Z_discrete_K,
        solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)
    Z_random = tf.argmax(epsilon_out, -1)
    epsilon = tf.reshape(epsilon, [batch_size, Z_discrete_D * Z_discrete_K])
    epsilon_out = tf.reshape(epsilon_out, [-1, Z_discrete_D * Z_discrete_K])
  else:
    epsilon = sampling.continuous_prior_sampling(
        batch_size, noises_dim, solo_dim=False, hparams=hparams)
    epsilon_out = sampling.continuous_prior_sampling(
        hparams.plot_batch_size, noises_dim,
        solo_dim=hparams.solo_dim_prior_sampling, hparams=hparams)
    Z_random = epsilon_out
  if noise_transform == "gen":
    with tf.variable_scope("G_Z_eps", reuse=not is_train):
      noises = nets.dense.eps2Z(epsilon, noises_dim, hparams, is_train=is_train)
    with tf.variable_scope("G_Z_eps", reuse=True):
      noises_out = nets.dense.eps2Z(
          epsilon_out, noises_dim, hparams, is_train=False)
  else:
    noises = epsilon  # (batch_size, d=D*K or noises_dim)
    noises_out = epsilon_out  # (plot_batch_size (may *2), d=D*K or noises_dim)
  noises = tf.cast(noises, tf.float32)
  noises_out = tf.cast(noises_out, tf.float32)

  #--- Auto-encoding model ---
  if not use_discrete_code or discrete_coding_scheme == "flat":
    X2Z_function = nets.hcode_flat.ae_X2Z
    Z2X_function = nets.hcode_flat.ae_Z2X
  elif discrete_coding_scheme == "vlae":
    X2Z_function = nets.hcode_vlae.X2Z
    Z2X_function = nets.hcode_vlae.Z2X
  elif discrete_coding_scheme == "args":
    X2Z_function = nets.hcode_args.X2Z
    Z2X_function = nets.hcode_args.Z2X
  elif discrete_coding_scheme == "args_dec":
    raise NotImplemented("Implementation needed, refer to discrete_vae.py")
  elif discrete_coding_scheme == "hyper":
    X2Z_function = nets.hcode_hyper.X2Z
    Z2X_function = nets.hcode_hyper.Z2X
  else:
    raise ValueError("Unknown discrete_coding_scheme {}".format(
        discrete_coding_scheme))
  X2Z_input = images
  noises_fake, noises_fake_prob, _, _, Z_KL_batch = X2Z_function(
      X2Z_input, None, hparams, is_train)  # noises_fake: (batch_size, d)
  if use_discrete_code:
    if len(noises_fake.shape.as_list()) != 2:
      noises_fake = tf.reshape(noises_fake,
                               [batch_size, Z_discrete_D * Z_discrete_K])
    codes = tf.cast(noises_fake, tf.int32)
  else:
    codes = noises_fake
  noises_fake = tf.cast(noises_fake, tf.float32)
  if Z_from_X:
    Z2X_input = tf.concat([noises_fake, labels], -1) if Z_use_label else noises_fake
  else:
    Z2X_input = tf.concat([noises, labels], -1) if Z_use_label else noises
  images_fake, X_loss_batch = Z2X_function(
      Z2X_input, images, hparams, is_train)

  # Add losses.
  if prototype != "vae":
    D_out_actv = tf.nn.sigmoid if loss_type == "logistic" else None
    D_X = tf.make_template(
        "D_X", nets.dense.D,
        hparams=hparams, output_activation=D_out_actv, is_train=is_train)
    D_Z = tf.make_template(
        "D_Z", nets.dense.D,
        hparams=hparams, output_activation=D_out_actv, is_train=is_train)
  dis_loss = gen_loss = tf.constant(0.)
  if XZ_joint_gan:
    if not X_use_gan or not Z_use_gan:
      raise ValueError("XZ_joint_gan only feasible when "
                       "X_use_gan and Z_use_gan are both True.")
    real = tf.concat([images, noises], 1)
    fake = tf.concat([images_fake, noises_fake], 1)
    score_real = D_X(real)
    score_fake = D_X(fake)
    dis_loss_xz, gen_loss_xz = get_gan_loss(
        loss_type, score_real, score_fake, D_X, real, fake)
    dis_loss += dis_loss_xz
    gen_loss += gen_loss_xz
  else:
    if prototype != "vae":
      X_score_real = D_X(images)
      X_score_fake = D_X(images_fake)
      Z_score_real = D_Z(noises)
      Z_score_fake = D_Z(noises_fake)
    if X_use_gan:
      dis_loss_x, gen_loss_x = get_gan_loss(
          loss_type, X_score_real, X_score_fake, D_X, images, images_fake)
      if is_train or hparams.test_summary:
        tf.summary.scalar("dis_loss_x", dis_loss_x)
        tf.summary.scalar("gen_loss_x", gen_loss_x)
      dis_loss += dis_loss_x
      gen_loss += gen_loss_x
    if Z_use_gan:
      dis_loss_z, gen_loss_z = get_gan_loss(
          loss_type, Z_score_real, Z_score_fake, D_Z, noises, noises_fake)
      if is_train or hparams.test_summary:
        tf.summary.scalar("dis_loss_z", dis_loss_z)
        tf.summary.scalar("gen_loss_z", gen_loss_z)
      dis_loss += dis_loss_z
      gen_loss += gen_loss_z
  X_reconst_loss = tf.reduce_mean(X_loss_batch)
  if is_train or hparams.test_summary:
    tf.summary.scalar("X_reconst_loss", X_reconst_loss)
  if X_use_reconst:
    gen_loss += X_reconst_loss * float(X_use_reconst)
  Z_KL = tf.reduce_mean(Z_KL_batch)
  if is_train or hparams.test_summary:
    tf.summary.scalar("Z_KL", Z_KL)
  if Z_use_KL:
    gen_loss += Z_KL * float(Z_use_KL)
  # Supervised loss.
  if hparams.weight_supervised_loss != 0:
    with tf.variable_scope("G_Z/Z2Y", reuse=not is_train):
      preds, super_loss = nets.dense.Z2Y(
          noises_fake, labels, hparams, is_train=is_train)
    gen_loss += super_loss * hparams.weight_supervised_loss
  else:
    preds = None
  # Adding regularization loss - DEBUG
  gen_loss += tf.reduce_sum(
      tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))

  if is_train:
    dis_theta = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/D_X")
    dis_theta += tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/D_Z")
    gen_theta = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_X")
    gen_theta += tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_Z")
    optimizer = util.get_optimizer(optimizer)
    optimizer = optimizer(learning_rate=learning_rate)

    if dis_theta:
      if dis_op_drop_rate == 0.:
        dis_loss_for_train = dis_loss
      else:
        dis_loss_for_train = dis_loss * tf.cast(
            tf.random_uniform([]) > dis_op_drop_rate, tf.float32)
      dis_train_op = tf.contrib.layers.optimize_loss(
          loss=dis_loss_for_train,
          global_step=tf.train.get_or_create_global_step(),
          learning_rate=learning_rate,
          variables=dis_theta,
          optimizer=optimizer,
          clip_gradients=max_grad_norm,
          name="Discriminator")
    else:
      dis_train_op = tf.constant(0., name="constant_noop")  # For e.g. VAE.
    if gen_op_drop_rate == 0.:
      gen_loss_for_train = gen_loss
    else:
      gen_loss_for_train = gen_loss * tf.cast(
          tf.random_uniform([]) > gen_op_drop_rate, tf.float32)
    gen_train_op = tf.contrib.layers.optimize_loss(
        loss=gen_loss_for_train,
        global_step=tf.train.get_or_create_global_step(),
        learning_rate=learning_rate,
        variables=gen_theta,
        optimizer=optimizer,
        clip_gradients=max_grad_norm,
        name="Generator")

    train_ops = {"D": dis_train_op,
                 "G": gen_train_op}
  else:
    train_ops = None

  losses = {"dis_loss": dis_loss,
            "gen_loss": gen_loss}
  noises_out = tf.concat([noises_out,labels], -1) if Z_use_label else noises_out
  images_random, _ = Z2X_function(
      noises_out, images, hparams, is_train=False)
  samples = {"Z_random": Z_random,
             "images_random": images_random,
             "images_reconst": images_fake,
             "labels": labels,
             "preds": preds,
             "codes": codes, # images, # directly use X/images as code.
             "reconst_loss": X_reconst_loss}

  return train_ops, losses, samples

