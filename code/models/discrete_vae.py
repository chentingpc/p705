import numpy as np
import tensorflow as tf
import nets
import sampling
import util
import pg
log = util.safer_log
from loss import get_gan_loss


def get_model(images,
              labels,
              loss_type,
              hparams,
              idxs=None,
              num_points=None,
              is_train=True):
  """Returns train_ops, losses, and sampled images.
  """
  optimizer = hparams.optimizer
  learning_rate = hparams.learning_rate
  max_grad_norm = hparams.max_grad_norm
  Z_use_KL = hparams.Z_use_KL
  X_use_reconst = hparams.X_use_reconst
  Z_discrete_K = hparams.Z_discrete_K  # K in KD code
  Z_discrete_D = hparams.Z_discrete_D  # D in KD code
  discrete_coding_scheme = hparams.discrete_coding_scheme
  assert Z_discrete_D == hparams.noises_dim  # To avoid bugs.

  # Be careful with changing the following setting.
  use_X2Z_net = True
  decoupled_reward = False
  santity_checker = False  # Test a toy example
  if santity_checker:
    Z_discrete_K = 2
    decoupled_reward = True
    t_val = 0.49  # minimal cost = t_val^2 = 0.2401.
    # Check gen_loss or score_meanin tensorboard.

  estimator_type = hparams.discrete_estimator
  if estimator_type == "gumbel":
    # Gumbel-Softmax
    pgor = None
    pg_num_samples = pg_first_n4f = 1  # Fixed.
  elif estimator_type == "policy":
    # Policy gradient.
    pg_num_samples = hparams.pg_num_samples
    pgor = pg.PolicyGradient(
        Z_discrete_K, Z_discrete_D, hparams,
        num_samples=pg_num_samples, one_hot_action=True)
    if pg_num_samples <= 0:
      if pg_num_samples == 0:
        raise ValueError("Use smaller than 0 number to indicate the number of "
                         "samples for training the f reward function.")
      pg_first_n4f = -pg_num_samples
      pg_num_samples = np.power(Z_discrete_K, Z_discrete_D) + pg_first_n4f
    else:
      pg_first_n4f = pg_num_samples
  elif estimator_type == "relax":
    # RELAX.
    pgor = pg.RebarRelax(Z_discrete_K, Z_discrete_D, hparams)
    pg_num_samples = 3  # Fixed, last two are baselines.
    pg_first_n4f = 1  # Fixed.
  else:
    raise ValueError()
  tf.logging.warn("Using estimator_type {}".format(estimator_type))

  #--- House keeping ---
  batch_size = tf.shape(images)[0]
  images_shape = images.shape.as_list()[1:]
  images_shape_flat = reduce(np.multiply, images_shape)
  images = tf.reshape(images, [-1, images_shape_flat])
  if pgor is None:  # Compute batch size for after X2Z.
    batch_size_new = batch_size
    images_new = images
  else:
    batch_size_new = pg_num_samples * batch_size
    images_new = tf.tile(images, [pg_num_samples, 1])  # (pg_num_samples * batch_size)
  if use_X2Z_net:
    X2Z_input = images
  else:
    X2Z_input = tf.one_hot(idxs, num_points)

  #--- Auto-encoding model ---
  if discrete_coding_scheme == "flat":
    X2Z_function = nets.hcode_flat.X2Z
    Z2X_function = nets.hcode_flat.Z2X
  elif discrete_coding_scheme == "vlae":
    X2Z_function = nets.hcode_vlae.X2Z
    Z2X_function = nets.hcode_vlae.Z2X
  elif discrete_coding_scheme == "args":
    X2Z_function = nets.hcode_args.X2Z
    Z2X_function = nets.hcode_args.Z2X
  elif discrete_coding_scheme == "args_dec":
    X2Z2X_function = nets.hcode_args.X2Z2X
    Z2X_function = nets.hcode_args.Z2X_of_X2Z2X
  elif discrete_coding_scheme == "hyper":
    X2Z_function = nets.hcode_hyper.X2Z
    Z2X_function = nets.hcode_hyper.Z2X
  else:
    raise ValueError("Unknown discrete_coding_scheme {}".format(
        discrete_coding_scheme))
  if discrete_coding_scheme == "args_dec":
    if pgor is not None:
      raise NotImplemented("Currently only supports Gumbel-Softmax.")
    Z, Z_prob, B, B_prior, Z_KL_batch, images_reconst, X_loss_batch = (
        X2Z2X_function(images, hparams, is_train))
  else:
    Z, Z_prob, B, B_prior, Z_KL_batch = X2Z_function(
        X2Z_input, pgor, hparams, is_train)
    images_reconst, X_loss_batch = Z2X_function(
        Z, images_new, hparams, is_train)
  
  #--- losses ---
  dis_loss = gen_loss = tf.constant(0.)
  # Reconstruction.
  X_reconst_loss = tf.reduce_mean(
      tf.reshape(X_loss_batch, [-1, batch_size])[:pg_first_n4f])
  if is_train or hparams.test_summary:
    tf.summary.scalar("X_reconst_loss", X_reconst_loss)
  if X_use_reconst:
    gen_loss += X_reconst_loss * float(X_use_reconst)
  # KL.
  Z_KL = tf.reduce_mean(
      tf.reshape(Z_KL_batch, [pg_num_samples, batch_size])[:pg_first_n4f])
  if is_train or hparams.test_summary:
    tf.summary.scalar("Z_KL", Z_KL)
  if Z_use_KL and pgor is None:
    gen_loss += Z_KL * float(Z_use_KL)
  # Supervised loss.
  if hparams.weight_supervised_loss != 0:
    with tf.variable_scope("G_Y", reuse=not is_train):
      if estimator_type == "gumbel":
        #Z2Y_input = Z[:, -1, :]  # DEBUG, using the last code alone
        Z2Y_input = tf.reshape(Z, [batch_size, Z_discrete_D * Z_discrete_K])
      elif estimator_type == "relax":
        # DEBUG: this negatively influence the performance.
        Z2Y_input = tf.reshape(Z[1], [batch_size, Z_discrete_D * Z_discrete_K])
      else:
        raise ValueError("No Z2Y for {}".format(estimator_type))
      preds, super_loss = nets.dense.Z2Y(
          Z2Y_input, labels, hparams, is_train=is_train)
    gen_loss += super_loss * hparams.weight_supervised_loss
  else:
    preds = None
  # Adding regularization loss - DEBUG
  gen_loss += tf.reduce_sum(
      tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
  # PG losses.
  if decoupled_reward:  # rewards based on latent code directly.
    if santity_checker:
      penalty_ = tf.reshape(tf.constant([t_val, 1.-t_val]), [1,1,Z_discrete_K])
      scores = tf.square(tf.reduce_sum(Z * penalty_, -1))
      if pgor is None:
        gen_loss = tf.reduce_mean(scores)
    else:
      labels_ = tf.tile(
          tf.expand_dims(tf.one_hot(labels, Z_discrete_K), 0),
          [tf.shape(Z)[0], 1, 1])
      # Scores purely from labels.
      #scores = -tf.cast(
      #    tf.equal(tf.argmax(Z, -1), tf.argmax(labels_, -1)),
      #    tf.float32)
      #scores = tf.reduce_sum(1 - labels_ * Z, -1)
      # Scores with prob.
      Z_prob = tf.reshape(Z_prob, [-1, batch_size, Z_discrete_K])
      scores = tf.reduce_sum((Z_prob - labels_) * Z, -1)
      #scores = tf.reduce_sum((Z - labels_) * Z, -1)
      if pgor is None:
        scores = tf.reduce_sum((Z - labels_)**2, -1)
        gen_loss = tf.reduce_mean(scores)
  else:
    scores = X_loss_batch * float(X_use_reconst) - (
        Z_KL_batch * float(Z_use_KL))
    scores = tf.reshape(scores, [-1, batch_size])
  if pgor is not None:
    score_mean = tf.reduce_mean(scores[:pg_first_n4f])
    if is_train or hparams.test_summary:
      tf.summary.scalar("score_mean", score_mean)
    if is_train:
      theta_params = tf.get_collection(
          tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_Z")
      pg_ops = pgor.backward(scores, theta_params=theta_params)

  if is_train:
    gen_theta = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_X")
    if pgor is None:
      gen_theta += tf.get_collection(
          tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_Z")
    gen_theta += tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope="model/G_Y")
    optimizer = util.get_optimizer(optimizer)
    optimizer = optimizer(learning_rate=learning_rate)

    gen_train_op = tf.contrib.layers.optimize_loss(
        loss=gen_loss,
        global_step=tf.train.get_or_create_global_step(),
        learning_rate=learning_rate,
        variables=gen_theta,
        optimizer=optimizer,
        clip_gradients=max_grad_norm,
        name="Generator")

    if Z_use_KL and pgor is not None:
      kl_train_op = tf.contrib.layers.optimize_loss(
          loss=Z_KL * float(Z_use_KL),
          global_step=tf.train.get_or_create_global_step(),
          learning_rate=learning_rate,
          variables=theta_params,
          optimizer=optimizer,
          clip_gradients=max_grad_norm,
          name="Z_KL")
      gen_train_op = tf.group(gen_train_op, kl_train_op)

    train_ops = {"D": tf.constant(0., name="constant_noop"),
                 "G": gen_train_op}
    if pgor is not None:
      train_ops["G"] = tf.group(pg_ops, train_ops["G"])
  else:
    train_ops = None

  losses = {"dis_loss": dis_loss,
            "gen_loss": gen_loss}
  images_random, _ = Z2X_function(
      tf.cast(B_prior, tf.float32), images, hparams, is_train=False)
  # Samples' batch size is batch_size.
  samples = {"Z_random": tf.argmax(B_prior, -1),  # (batch_size, D)
             "images_random": images_random,
             "images_reconst": images_reconst[:batch_size],
             "labels": labels,
             "preds": preds,
             "codes": tf.reshape(B, [batch_size, Z_discrete_D * Z_discrete_K]),
             "reconst_loss": X_reconst_loss}

  return train_ops, losses, samples
