import numpy as np
import tensorflow as tf
from datasets import mnist, cifar, ann
import matplotlib as mpl
mpl.use('agg')  # for the server.
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


def get_image_data(data_name, data_dir, is_training,
                   batch_size, num_epochs, reinitializer, data_format):
  if data_name == "mnist":
    images, labels, initializer = mnist.input_fn(
        is_training, data_dir, batch_size,
        num_epochs, reinitializer, data_format)
  elif data_name in ["cifar10", "cifar100"]:
    images, labels, initializer = cifar.input_fn(
        data_name == "cifar10", is_training, data_dir,
        batch_size, num_epochs, reinitializer, data_format)
  else:
    raise ValueError("no data named {}".fromat(data_name))
  return images, labels, initializer


def get_feature_data(data_name, data_dir, is_training,
                     batch_size, num_epochs, reinitializer):
  if data_name in ["sift10k", "sift1m", "gist1m"]:
    features, initializer = ann.input_fn(
        data_name, is_training, data_dir,
        batch_size, num_epochs, reinitializer)
    labels = tf.zeros([tf.shape(features)[0]])
    with tf.variable_scope("data_bn", reuse=not is_training):
      features = tf.layers.batch_normalization(features, training=is_training)
  else:
    raise ValueError("no data named {}".fromat(data_name))
  return features, labels, initializer


def plot_images(samples, rgb=False, filename=None,
                fsize=None, wspace=0.05, hspace=0.05):
  ''' samples: 2d array of N x wh^2
  '''
  if isinstance(samples.flatten()[0], np.floating):
    # samples -= np.min(samples)
    # samples /= np.max(samples)
    # samples = (255.99*samples).astype('uint8')
    pass
  if fsize is None:
    N = samples.shape[0]
    nsqrt = int(np.sqrt(N))
    fsize = (nsqrt, nsqrt)
    samples[:nsqrt**2]
  if rgb:
    wh = int(np.sqrt(samples[0].shape[0] / 3))
  else:
    wh = int(np.sqrt(samples[0].shape[0]))
  fig = plt.figure(figsize=fsize)
  gs = gridspec.GridSpec(*fsize)
  gs.update(wspace=wspace, hspace=hspace)

  for i, sample in enumerate(samples):
      ax = plt.subplot(gs[i])
      plt.axis('off')
      ax.set_xticklabels([])
      ax.set_yticklabels([])
      ax.set_aspect('equal')
      if rgb:
        sample = sample.reshape(wh, wh, 3)
        plt.imshow(sample, cmap='Greys_r')
      else:
        sample = sample.reshape(wh, wh)
        plt.imshow(sample, cmap='Greys_r')

  if filename:
    plt.savefig(filename + ".png", bbox_inches="tight")

  plt.close()
  return fig

