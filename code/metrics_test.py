import numpy as np
import metrics
import tensorflow as tf


class TestGumbel(tf.test.TestCase):
  def _case_test(self, foo_xyks, X, y, ks, precisions_exp, recalls_exp):
    results = foo_xyks(X, y, ks, return_cases=None)
    precisions, recalls = results["precisions"], results["recalls"]
    self.assertAllClose(precisions, precisions_exp, rtol=1e-4, atol=1e-4)
    self.assertAllClose(recalls, recalls_exp, rtol=1e-4, atol=1e-4)

  def testFindKNNs(self):
    # Case 1.
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    result = metrics.find_knns(X, 0).reshape([-1]).tolist()
    self.assertEquals(result, [0,1,2,3])
    # Case 2.
    X = np.array([[0, 0], [0, 1], [1, 2]])
    result = metrics.find_knns(X, 1).tolist()
    self.assertEquals(result, [[0, 1], [1, 0], [2, 1]])

  def testKNN(self):
    foo_xyks = metrics.knn_eval_by_labels
    # Case 1.
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y = [1, 0, 0, 1]
    ks = [2, 3, 4]
    precisions_exp = [0.0, 0.3333, 0.3333, 0.3333]
    recalls_exp = [0.0, 1, 1, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)
    # Case 2.
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y = [1, 1, 1, 1]
    ks = [2, 3, 4]
    precisions_exp = [1, 1, 1, 1]
    recalls_exp = [0.6666, 1, 1, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)
    # Case 3.
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y = [0, 0, 1, 1]
    ks = [2, 3, 4]
    precisions_exp = [0.5, 0.3333, 0.3333, 0.3333]
    recalls_exp = [1, 1, 1, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)

  def testKNNIdxs(self):
    foo_xyks = metrics.knn_eval_by_labels
    # Case 1.
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y = [[0, 1], [1, 0], [2, 0], [3, 1]]
    ks = [1, 2, 3]
    precisions_exp = [1, 0.5, 0.33333, 0.33333]
    recalls_exp = [1, 1, 1, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)

  def testHKNN(self):
    foo_xyks = metrics.hknn_eval_by_labels
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y = [1, 0, 0, 1]
    # ks = [1, 2, 3]
    ks = [2]
    precisions_exp = [0.25, 0.3333]
    recalls_exp = [0.5, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)
    # Case 2.
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y = [1, 1, 1, 1]
    #ks = [1, 2, 4]
    ks = [2]
    precisions_exp = [1, 1]
    recalls_exp = [0.6666, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)
    # Case 3.
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y = [0, 0, 1, 1]
    #ks = [1, 2, 4]
    ks = [2]
    precisions_exp = [0.25, 0.3333]
    recalls_exp = [0.5, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)

  def testHKNNGivenNeighbors(self):
    foo_xyks = metrics.hknn_eval_by_labels
    X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
    y = [[0, 1], [1, 0], [2, 0], [3, 1]]
    #ks = [1, 2, 3]
    ks = [2]
    precisions_exp = [0.375, 0.3333]
    recalls_exp = [0.75, 1]
    self._case_test(foo_xyks, X, y, ks, precisions_exp, recalls_exp)

if __name__ == "__main__":
  tf.test.main()
